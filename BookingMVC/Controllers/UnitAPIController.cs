﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Services;
using BookingMVC.Services.ContractsServices;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookingMVC.Controllers
{
    public class UnitAPIController : ApiController
    {
        private readonly IUnitService _unitService;
        public UnitAPIController(IUnitService unitService)
        {
            _unitService = unitService;
        }

        [Route("api/UnitAPI/GetUnits")]
        [HttpPost]
        public HttpResponseMessage GetUnits([FromBody] string search)
        {
            var units = _unitService.SearchList(search);
            return Request.CreateResponse(HttpStatusCode.OK, units);
        }
    }
}
