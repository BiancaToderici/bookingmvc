﻿using BookingMVC.Filters;
using BookingMVC.Models;
using BookingMVC.Services.ContractsServices;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BookingMVC.Controllers
{ 
    [AuthorizationWebAPI]
    public class UserAPIController : ApiController
    {
        private readonly IUserService _userService;
        public UserAPIController(IUserService userService)
        {
            _userService = userService;
        }
        
        [Route("api/UserAPI/CreateUser")]
        [HttpPost]
        public HttpResponseMessage CreateUser([FromBody] UserModel userModel)
        {
            bool userExists = _userService.UserExists(userModel.UserName);

            if (!userExists)
            {
                try
                {
                    _userService.Add(userModel);
                    return Request.CreateResponse(HttpStatusCode.OK, "User saved successfully");
                }
                catch (Exception e)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User Name already exists");
            }
        }

        [Route("api/UserAPI/EditUser")]
        [HttpPut]
        public HttpResponseMessage EditUser([FromBody] UserModel userModel)
        {
            try
            {
                _userService.Edit(userModel);
                return Request.CreateResponse(HttpStatusCode.OK, "User updated successfully");
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [Route("api/UserAPI/Delete")]
        [HttpGet]
        public HttpResponseMessage Delete(int userId)
        {
            _userService.Delete(userId);
            var newUrl = Url.Link("Default", new { Controller = "User", Action = "Index" });
            var response = Request.CreateResponse(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri(newUrl);
            return response;
        }
    }
}
;