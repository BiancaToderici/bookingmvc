﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Services;
using BookingMVC.Services.ContractsServices;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserService _userService;
        public LoginController(IUserService service)
        {
            _userService = service;
        }

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Authorize(UserModel userModel)
        {
            var user = new User { UserName = userModel.UserName, Password = userModel.Password };
            bool hasAccess = _userService.IsAuthorized(ref user);

            if (!hasAccess)
            {
                ViewBag.Message = "Wrong username or password";

                return View("Login", userModel);
            }
            else
            {
                Session["User"] = user;
                
                return RedirectToAction("Index","Home");
            }
        }

        public ActionResult Logout()
        {
            Session["User"] = null;
            return RedirectToAction("Login", "Login");
        }
    }
}