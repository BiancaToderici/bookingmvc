﻿using BookingMVC.Common;
using BookingMVC.Common.Logger;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    public class UnitController : Controller
    {
        private readonly IRoomService _roomService;
        private readonly IUnitService _unitService;
        private readonly IPhotoService _photoService;
        private readonly IRatingService _ratingService;
        private readonly ILogBase _logBase;
        private readonly ILogFactory _logFactory;
        public UnitController(IRoomService roomService,IUnitService unitService, IPhotoService photoService, IRatingService ratingService, ILogFactory logFactory)
        {
            _roomService = roomService;
            _unitService = unitService;
            _photoService = photoService;
            _ratingService = ratingService;
            _logFactory = logFactory;
            _logBase = logFactory.GetLogger(LoggerType.File);
        }
        // GET: Unit
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(UnitModel unitModel)
        {
            bool unitexists = _unitService.UnitExist(unitModel.Unit.Name);

            if (!unitexists)
            {
                try
                {
                    User user = (User)Session["User"];
                    unitModel.Unit.UserId = user.Id;
                    _unitService.Create(unitModel);
                    ViewBag.Message = "Success";
                    ModelState.Clear();
                    return View();
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = $"Error: {e.Message}";
                    _logBase.Error(e.Message,e.StackTrace);
                    return View(unitModel);
                }
            }
            else
            {
                ViewBag.MessageError = "This Unit already exists";
                return View();
            }
        }
        
        public ActionResult AddRoom(int unitId)
        {
            ViewBag.UnitId = unitId;
            return View();
        }
        
        [HttpPost]
        public ActionResult AddRoom(RoomModel roomModel,int unitId)
        {
            bool roomNrExists = _roomService.RoomNrExists(roomModel.RoomNr,roomModel.UnitId);

            if (!roomNrExists)
            {
                try
                {
                    ViewBag.UnitId = unitId;
                    _roomService.AddRoom(roomModel);
                    ViewBag.Message = "Successfuly added";
                    ModelState.Clear();
                    return View();
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = $"Error: {ex.Message}";
                    _logBase.Error(ex.Message,ex.StackTrace);
                    return View(roomModel);
                }
            }
            else
            {
                ViewBag.MessageError = "Room Number already exists";
                return View();
            }
            
        }
        
        [HttpGet]
        public ActionResult Edit(int unitId)
        {
            ViewBag.UnitId = unitId;
            var unit = _unitService.UnitById(unitId);
            var unitModel = new UnitModel
            {
                Unit = unit
            };
            return View(unitModel);
        }

        [HttpPost]
        public ActionResult Edit(UnitModel unitModel)
        {
            try
            {
                _unitService.Edit(unitModel);
                ViewBag.Message = "Saved";
                return View(unitModel);
            }
            catch(Exception e)
            {
                ViewBag.Error = $"Error: {e.Message}";
                return View(unitModel);
            }
        }
        
        [HttpGet]
        public ActionResult AddPhoto(int unitId)
        {
            ViewBag.UnitId = unitId;
            return View();
        }
        
        [HttpPost]
        public ActionResult AddPhoto(int unitId, IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                foreach (var file in files)
                {
                    _photoService.AddPhoto(file, PhotoType.Hotel, unitId);
                }

                ViewBag.Message = ($"{files.Count()} photos uploaded successfully");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = $"Error : {e.Message}";
            }
            return View();
        }
        
        [HttpGet]
        public ActionResult MyReviews(int userId)
        {
            ViewBag.UserId = userId;
            ReviewsHistoryModel model = new ReviewsHistoryModel();
            var reviews = _ratingService.GetByUser(userId);
            model.Ratings = reviews;
            return View(model);
        }

        
        [HttpGet]
        public ActionResult AddReview(int bookingId)
        {
            User user = (User)Session["User"];
            var userId = user.Id;
            ViewBag.RatingLimit = 5;
            ViewBag.BookingId = bookingId;
            return View();
        }
        
        [HttpPost]
        public ActionResult AddReview(RatingModel ratingModel,int bookingId)
        {
            try
            {
                User user = (User)Session["User"];
                ratingModel.UserId = user.Id;
                _ratingService.Add(ratingModel);

                ViewBag.Message = "Thank you for posting a review!";
            }
            catch(Exception e)
            {
                ViewBag.Error = $"Error : {e.Message}";
            }
            
            return View();
        }
        
        [HttpGet]
        public ActionResult EditReview(int id)
        {
            var ratingModel = new RatingModel();
            ViewBag.EditMode = true;
            ViewBag.RatingLimit = 5;
            var rating = _ratingService.GetById(id);
            if (rating != null)
            {
                ratingModel.Id = rating.Id;
                ratingModel.Details = rating.Details;
                ratingModel.BookingId = rating.BookingId;
                ratingModel.UserId = rating.UserId;
                ratingModel.Value = rating.Value.Value;
            }
            else
            {
                var reviews = _ratingService.GetByBooking(id);
                foreach (var review in reviews)
                {
                    ratingModel.Id = review.Id;
                    ratingModel.Details = review.Details;
                    ratingModel.BookingId = review.BookingId;
                    ratingModel.UserId = review.UserId;
                    ratingModel.Value = review.Value.Value;
                }
            }

            return View("AddReview", ratingModel);
        }
        
        [HttpPost]
        public ActionResult EditReview(RatingModel ratingModel)
        {
            try
            {
                ViewBag.EditMode = true;
                _ratingService.Edit(ratingModel);
                ViewBag.Message = "Saved";
            }
            catch (Exception e)
            {
                ViewBag.Error = $"Error: {e.Message}";;
            }
            return View("AddReview",ratingModel);
        }

        [HttpPost]
        public ActionResult DeleteReview(int id, int userId)
        {
            _ratingService.Delete(id);
            return RedirectToAction("MyReviews", new { userId = userId });
        }
    }
}