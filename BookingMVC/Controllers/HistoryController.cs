﻿using BookingMVC.Repositories;
using BookingMVC.Services;
using BookingMVC.Services.ContractsServices;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    public class HistoryController : Controller
    {
        private readonly IHistoryService _historyService;
        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }
        // GET: History
        /// <summary>
        /// Get Rooms Booked By User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ActionResult Show(int userId)
        {
            var user = (User)Session["User"];
            user.Id = userId;
            var rooms = _historyService.RoomsBooked(userId);
            return View(rooms);
        }
    }
}