﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Services;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPhotoService _photoService;
        private readonly IRoomService _roomService;
        private readonly IUnitService _unitService;
        private readonly IRatingService _ratingService;
        private readonly IRoomBookingService _roomBookingService;

        public HomeController(IPhotoService photoService, IRoomService roomService, IUnitService unitService,IRatingService ratingService,IRoomBookingService roomBookingService)
        {
            _photoService = photoService;
            _roomService = roomService;
            _unitService = unitService;
            _ratingService = ratingService;
            _roomBookingService = roomBookingService;
        }
        /// <summary>
        /// Get all Units from database
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            UnitIndexModel model = new UnitIndexModel();
            var unitModelList = new List<UnitModel>();
            var units = _unitService.GetUnits();
            foreach (var unit in units)
            {
               var rating = _ratingService.GetAverage(unit.Id);

                var unitModel = new UnitModel
                {
                    Unit = unit,
                    Rating = string.Format("{0:0.0}", rating)
                };

                unitModelList.Add(unitModel);
            }

            model.Units = unitModelList;
            return View(model);
        }

        /// <summary>
        /// Search field by name or addres,check-in and check-out
        /// </summary>
        /// <param name="search"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="unitId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string search,DateTime? dateFrom, DateTime? dateTo)
        {
            UnitIndexModel model = new UnitIndexModel();
            var unitModelList = new List<UnitModel>();
            Session["DateFrom"] = dateFrom;
            Session["DateTo"] = dateTo;
            var units = _unitService.GetFreeUnits(search,dateFrom,dateTo);
            foreach (var unit in units)
            {
                var rating = _ratingService.GetAverage(unit.Id);
                var unitModel = new UnitModel
                {
                    Unit = unit,
                    Rating = string.Format("{0:0.0}", rating)
                };

                unitModelList.Add(unitModel);
            }
            model.Units = unitModelList;
            return View(model);
        }

        /// <summary>
        /// Get all offers
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ActionResult Offers(int userId)
        {
            ViewBag.UserId = userId;
            var offers = _unitService.UserOffers(userId);
            return View(offers);
        }

        /// <summary>
        /// Delete an Unit
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int unitId)
        {
            _unitService.Delete(unitId);
            return RedirectToAction("Index");
        }
        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        } 
    }
}