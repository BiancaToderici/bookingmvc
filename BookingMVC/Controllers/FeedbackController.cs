﻿using BookingMVC.Common;
using BookingMVC.Filters;
using BookingMVC.Models;
using BookingMVC.Services.ContractsServices;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    [Authentication]
    public class FeedbackController : Controller
    {
        private IFeedbackService _feedbackService;
        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var feedbacks = _feedbackService.Get().Result;
            return View(feedbacks);
        }
        
        [HttpPost]
        public ActionResult Index(string search)
        {
            var feedbacks = _feedbackService.GetByUserName(search).Result;
            return View(feedbacks);
        }

        [HttpGet]
        public JsonResult Search(string search)
        {
            var names = _feedbackService.Search(search).Result;
            return Json(names, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create(int userId, string userName)
        {
            ViewBag.UserId = userId;
            ViewBag.UserName = userName;
            return View();
        }

        [HttpPost]
        public ActionResult Create(FeedbackModel feedbackModel)
        {
            _feedbackService.Create(feedbackModel);
            return View(feedbackModel);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            ViewBag.EditMode = true;
            var feedback = _feedbackService.GetById(id).Result;
            ViewBag.UserId = feedback.UserId;
            ViewBag.UserName = feedback.Name;
            return View("Create", feedback);
        }
        
        [HttpPost]
        public ActionResult Edit(Feedback feedback)
        {
            _feedbackService.Edit(feedback);
            return View("Create", feedback);
        }

        [HttpGet]
        public ActionResult Delete(string id)
        {
            _feedbackService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}