﻿using BookingMVC.Filters;
using BookingMVC.Models;
using BookingMVC.Services.ContractsServices;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    [Authorization]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var users = _userService.GetList();
            var usersList = new List<UserModel>();
            foreach (var item in users)
            {
                UserModel userModel = new UserModel
                {
                    Id = item.Id,
                    UserName = item.UserName,
                    Password = item.Password,
                    Type = item.Type
                };
                usersList.Add(userModel);
            }
            return View(usersList);
        }
        
        public ActionResult Create()
        {
            return View(new UserModel());
        }
        
        [HttpGet]
        public ActionResult Edit(int userId)
        {
            ViewBag.EditMode = true;
            var user = _userService.GetById(userId);
            var userModel = new UserModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Password = user.Password,
                Type = user.Type
            };
            return View("Create", userModel);
        }
    }
}