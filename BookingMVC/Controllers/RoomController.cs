﻿using BookingMVC.Common;
using BookingMVC.Common.Logger;
using BookingMVC.Filters;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingMVC.Controllers
{
    [Authentication]
    public class RoomController : Controller
    {
        private readonly IHistoryService _historyService;
        private readonly IPhotoService _photoService;
        private readonly IRoomBookingService _roomBookingService;
        private readonly IRoomService _roomService;
        private readonly IUnitService _unitService;
        private readonly IRatingService _ratingService;
        private readonly IUserService _userService;
        private readonly ILogBase _logBase;
        private readonly ILogFactory _logFactory;

        public RoomController(IHistoryService service,IPhotoService photoService,IRoomBookingService roomBookingService,IRoomService roomService,IUnitService unitService,IRatingService ratingService, IUserService userService, ILogFactory logFactory)
        {
            _historyService = service;
            _photoService = photoService;
            _roomBookingService = roomBookingService;
            _roomService = roomService;
            _unitService = unitService;
            _ratingService = ratingService;
            _userService = userService;
            _logFactory = logFactory;
            _logBase = logFactory.GetLogger(LoggerType.Database);
        }
        // GET: Room
        /// <summary>
        /// Get all rooms available foreach Unit
        /// </summary>
        /// <param name="id"> UnitId </param>
        /// <param name="dateFrom"> Check-in Date </param>
        /// <param name="dateTo"> Check-out Date</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(int id)
        {
            RoomIndexModel roomIndexModel = new RoomIndexModel();
            var ratingModelList = new List<RatingModel>();
            DateTime? dateFrom = (DateTime?)Session["DateFrom"];
            DateTime? dateTo = (DateTime?)Session["DateTo"];

            if (dateFrom.HasValue && dateTo.HasValue)
            {
                roomIndexModel.Rooms = _roomService.GetFreeRooms(dateFrom, dateTo, id);
                Session.Remove("DateFrom");
                Session.Remove("DateTo");
                ViewBag.DateFrom = dateFrom;
                ViewBag.DateTo = dateTo;
            }
            else
            {
                roomIndexModel.Rooms = _roomService.GetRooms(id);
            }

            roomIndexModel.Photos = _photoService.DisplayPhoto(id, PhotoType.Hotel);
            var ratings = _ratingService.GetByUnit(id);
            foreach (var rating in ratings)
            {
                var userName = _userService.GetNameById(rating.UserId);
                var ratingModel = new RatingModel
                {
                    Date = rating.Date,
                    Details = rating.Details,
                    BookingId = rating.BookingId,
                    UserId = rating.UserId,
                    Value = rating.Value,
                    UserName = userName
                };

                ratingModelList.Add(ratingModel);
            }
            roomIndexModel.Ratings = ratingModelList;
            var average = _ratingService.GetAverage(id);
            roomIndexModel.Average = string.Format("{0:0.0}", average);
            roomIndexModel.UnitName = _unitService.GetNameById(id);
            return View(roomIndexModel);
        }

        [HttpPost]
        public ActionResult Index(DateTime? dateFrom,DateTime? dateTo,int? id)
        {
            var average = _ratingService.GetAverage(id.GetValueOrDefault());
            RoomIndexModel roomIndexModel = new RoomIndexModel
            {
                Rooms = _roomService.GetFreeRooms(dateFrom, dateTo, id),
                Photos = _photoService.DisplayPhoto(id.GetValueOrDefault(), PhotoType.Hotel),
                Average = string.Format("{0:0.0}", average),
                Ratings = _ratingService.GetByUnit(id.GetValueOrDefault())
            };
            roomIndexModel.UnitName = _unitService.GetNameById(id.GetValueOrDefault());
            return View(roomIndexModel);
        }

        /// <summary>
        /// Get pictures of rooms
        /// </summary>
        /// <param name="subjectId"> Id of Units or Rooms</param>
        /// <returns></returns>
        public ActionResult Picture(int subjectId)
        {
            ViewBag.subjectId = subjectId;
            RoomPictureModel roomPictureModel = new RoomPictureModel();
            roomPictureModel.Photos = _photoService.DisplayPhoto(subjectId, Common.PhotoType.Room);
            roomPictureModel.RoomNr = _roomService.GetRoomNr(subjectId);
            return View(roomPictureModel);
        }
        
        [HttpGet]
        public ActionResult Book(int roomId)
        {
            ViewBag.RoomId = roomId;
            return View();
        }

        /// <summary>
        /// Book a room
        /// </summary>
        /// <param name="roomBookingModel"></param>
        /// <param name="roomId"></param>
        /// <param name="historyModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Book(RoomBookingModel roomBookingModel, int roomId, HistoryModel historyModel)
        {
            try
            {
                User user = (User)Session["User"];
                roomBookingModel.UserId = user.Id;
                historyModel.RoomId = roomId;
                historyModel.UserId = user.Id;
                roomBookingModel.RoomId = roomId;
                _roomBookingService.BookARoom(roomBookingModel);
                _historyService.RoomHistory(historyModel);
                ViewBag.Success = "Room Booked";
                return View(roomBookingModel);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = $"Error : {ex.Message}";
                _logBase.Error(ex.Message,ex.StackTrace);
                return View(roomBookingModel);
            }
        }
        
        [HttpGet]
        public ActionResult Edit(int roomId)
        {
            ViewBag.RoomId = roomId;
            var room = _roomService.GetById(roomId);
            var roomModel = new RoomModel
            {
                Details = room.Details,
                NrPersons = room.NrPersons,
                Price = room.Price,
                RoomNr = room.RoomNr,
                Type = room.Type,
                UnitId = room.UnitId
            };

            return View(roomModel);
        }
        
        [HttpPost]
        public ActionResult Edit(RoomModel roomModel, int roomId)
        {
            ViewBag.RoomId = roomId;
            try
            {
                _roomService.Edit(roomModel, roomId);
                ViewBag.Message = "Saved";
                return View(roomModel);
            }
            catch (Exception e)
            {
                ViewBag.Error = $"Error: {e.Message}";
                return View(roomModel);
            }
        }
        
        [HttpGet]
        public ActionResult AddPhoto(int roomId)
        {
            ViewBag.RoomId = roomId;
            return View();
        }
        
        [HttpPost]
        public ActionResult AddPhoto(int roomId,IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                foreach (var file in files)
                {
                    _photoService.AddPhoto(file, PhotoType.Room, roomId);
                }
                ViewBag.Message = ($"{files.Count()} photos uploaded successfully");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = $"Error : {e.Message}";
            }
            return View();
        }

        /// <summary>
        /// Delete a room
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="unitId"> UnitId </param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int roomId,int unitId)
        {
            _roomService.Delete(roomId);
            return RedirectToAction("Index", new { id = unitId});
        }
    }
}