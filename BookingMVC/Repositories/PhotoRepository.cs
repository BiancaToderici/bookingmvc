﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace BookingMVC.Repositories
{
    public class PhotoRepository : ApplicationConfiguration, IPhotoRepository
    {
        public IEnumerable<Photo> GetPhotos(int subjectId,PhotoType photoType)
        {
            using (var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                List<Photo> list = db.Photos.Where(p => p.SubjectId == subjectId && p.Type == photoType).ToList();
                return list;
            }
        }

        public void AddPhoto(Photo photo)
        {
            using(var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                db.Photos.Add(photo);
                db.SaveChanges();
            }
        }
    }
}