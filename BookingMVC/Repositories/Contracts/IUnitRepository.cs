﻿using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Repositories.Contracts
{
    public interface IUnitRepository
    {
        IEnumerable<Unit> GetList();
        IEnumerable<Unit> GetUnits(string search);
        IEnumerable<Unit> UserOffers(int userId);
        Unit GetName(string unitName);
        string GetNameById(int id);
        Unit GetById(int unitId);
        void Add(Unit unit);
        void Edit(Unit unit);
        void DeleteUnit(int id);
    }
}