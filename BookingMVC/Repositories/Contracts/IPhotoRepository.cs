﻿using BookingMVC.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Repositories.Contracts
{
     public interface IPhotoRepository
    {
        IEnumerable<Photo> GetPhotos(int subjectId, PhotoType photoType);
        void AddPhoto(Photo photo);
    }
}
