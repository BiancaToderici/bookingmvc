﻿using BookingMVC.Common;

namespace BookingMVC.Repositories.Contracts
{
    public interface ILogRepository
    {
        void Save(Log log);
    }
}
