﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Repositories.Contracts
{
    public interface IUserRepository
    {
        User GetUser(string userName);
        string GetNameById(int id);
        void Add(User user);
        IEnumerable<User> GetList();
        void Edit(User user);
        void Delete(int id);
        User GetById(int id);
    }
}
