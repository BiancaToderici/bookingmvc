﻿using BookingMVC.Common;
using BookingMVC.Models;
using System.Collections.Generic;

namespace BookingMVC.Repositories.Contracts
{
    public interface IRatingRepository
    {
        void Add(Rating rating);
        IEnumerable<Rating> GetRatings(int bookingId);
        void Edit(Rating rating);
        void Delete(int id);
        IEnumerable<Rating> GetUserRatings(int userId);
        double? AverageRatings(IEnumerable<RatingModel> ratings);
        Rating GetById(int id);
        IEnumerable<RatingModel> GetUnitRatings(int unitId);
        IEnumerable<RatingModel> GetUserRatingsExtended(int userId);
        IEnumerable<RatingModel> GetBookingRatingsExtended(int bookingId);
    }
}
