﻿using BookingMVC.Common;
using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Repositories.Contracts
{
    public interface IRoomBookingRepository
    {
        IEnumerable<RoomBooking> GetRoomsBooked();
        RoomBooking BookRoom(RoomBookingModel roomBookingModel);
    }
}
