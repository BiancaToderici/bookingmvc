﻿using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Repositories.Contracts
{
    public interface IRoomRepository
    {
        IEnumerable<Room> GetRooms(int unitId);
        Room GetById(int roomId);
        Room GetNr(int roomNr, int unitId);
        int RoomNr(int id);
        Room AddRoom(RoomModel roomModel);
        void Edit(Room room);
        IEnumerable<Room> GetFreeRooms(DateTime? dateFrom, DateTime? dateTo, int? unitId);
        void Delete(int id);
        bool IsBooked(Room room, DateTime? dateFrom, DateTime? dateTo);
    }
}
