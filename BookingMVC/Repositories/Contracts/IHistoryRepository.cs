﻿using BookingMVC.Common;
using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Repositories.Contracts
{
     public interface IHistoryRepository
    {
        void SaveHistory(History history);
        IEnumerable<RoomBookingHistory> History(int userId);
    }
}
