﻿using BookingMVC.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class HistoryDbContext : DbContext
    {
        public DbSet<History> History { get; set; }

        public HistoryDbContext(string connectionstring) : base(connectionstring)
        {
        }
    }
}