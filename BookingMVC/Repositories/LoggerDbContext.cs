﻿using BookingMVC.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class LoggerDbContext : DbContext
    {
        public DbSet<Log> Log { get; set; }
        public LoggerDbContext(string connectionstring) : base(connectionstring) { }
    }
}