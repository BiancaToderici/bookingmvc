﻿using BookingMVC.Common;
using System.Data.Entity;

namespace BookingMVC.Repositories
{
    public class RoomDbContext : DbContext
    {
        public DbSet<Room> Rooms { get; set; }
        public RoomDbContext(string connectionstring) : base(connectionstring)
        {
        }
    }
}