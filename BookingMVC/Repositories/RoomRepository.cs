﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingMVC.Repositories
{
    public class RoomRepository : ApplicationConfiguration, IRoomRepository
    {
        public IEnumerable<Room> GetRooms(int unitId)
        {
            using (var roomDbContext = new RoomDbContext(BookingConnectionString))
            {
                IEnumerable<Room> rooms = roomDbContext.Rooms.Where(r => r.UnitId == unitId).ToList();

                return rooms;
            }
        }

        public Room GetById(int roomId)
        {
            using (var db = new RoomDbContext(BookingConnectionString))
            {
                var room = db.Rooms.Where(r => r.Id == roomId).SingleOrDefault();
                return room;
            }
        }

        public Room GetNr(int roomNr,int unitId)
        {
            using(var db = new RoomDbContext(BookingConnectionString))
            {
                Room room = db.Rooms.Where(r => r.RoomNr == roomNr && r.UnitId == unitId).FirstOrDefault();
                return room;

            }
        }

        public int RoomNr(int id)
        {
            using (var db = new RoomDbContext(BookingConnectionString))
            {
                var room = db.Rooms.Where(r => r.Id == id).Select(r => r.RoomNr).SingleOrDefault();
                return room;
            }
        }
        public Room AddRoom(RoomModel roomModel)
        {
            using (var db = new RoomDbContext(BookingConnectionString))
            {
                Room room = new Room();
                room.RoomNr = roomModel.RoomNr;
                room.Type = roomModel.Type;
                room.NrPersons = roomModel.NrPersons;
                room.Details = roomModel.Details;
                room.Price = roomModel.Price;
                room.UnitId = roomModel.UnitId;

                db.Rooms.Add(room);
                db.SaveChanges();

                return room;
            }
        }

        public void Edit(Room room)
        {
            using (var db = new RoomDbContext(BookingConnectionString))
            {
                Room existingRoom = db.Rooms.Where(u => u.Id == room.Id).SingleOrDefault();

                if (existingRoom != null)
                {
                    existingRoom.RoomNr = room.RoomNr;
                    existingRoom.Type = room.Type;
                    existingRoom.NrPersons = room.NrPersons;
                    existingRoom.Details = room.Details;
                    existingRoom.Price = room.Price;
                }
                db.SaveChanges();
            }
        }

        public IEnumerable<Room> GetFreeRooms(DateTime? dateFrom,DateTime? dateTo, int? unitId)
        {
            using (var db = new RoomDbContext(BookingConnectionString))
            {
                IList<Room> freeRooms = new List<Room>();
                if (unitId.HasValue)
                {
                    var allRooms = db.Rooms.Where(r => r.UnitId == unitId).ToList();
                    
                    foreach (var item in allRooms)
                    {
                        bool isBooked = IsBooked(item, dateFrom, dateTo);
                        if (!isBooked)
                        {
                            freeRooms.Add(item);
                        }
                    }
                }
                    return freeRooms;
            }
        }

        public bool IsBooked(Room room, DateTime? dateFrom, DateTime? dateTo)
        {
            using (var db = new RoomBookingDbContext(BookingConnectionString))
            {
                bool hasBookings = db.RoomsBooking.Any(d => d.DateFrom <= dateFrom && d.DateTo >= dateTo && d.RoomId == room.Id);
                return hasBookings;
            }
        }

        public void Delete(int id)
        {
            using (var db = new RoomDbContext(BookingConnectionString))
            {
                Room room = db.Rooms.SingleOrDefault(u => u.Id == id);
                if (room != null)
                {
                    db.Rooms.Remove(room);
                    db.SaveChanges();
                }
            }
        }
    }
}