﻿using System.Data.Entity;

namespace BookingMVC.Repositories
{
    public class UnitDbContext : DbContext
    {
       
        public DbSet<Unit> Units { get; set; }

        public UnitDbContext(string connectionstring) : base(connectionstring)
        {
        }
    }
}