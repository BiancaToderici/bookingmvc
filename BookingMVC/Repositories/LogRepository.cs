﻿using BookingMVC.Common;
using BookingMVC.Repositories.Contracts;

namespace BookingMVC.Repositories
{
    public class LogRepository : ApplicationConfiguration , ILogRepository
    {
        public void Save(Log log)
        {
            using( var db = new LoggerDbContext(LoggerConnectionString))
            {
                db.Log.Add(log);
                db.SaveChanges();
            }
        }
    }
}