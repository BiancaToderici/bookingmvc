﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace BookingMVC.Repositories
{
    public class RatingRepository : ApplicationConfiguration, IRatingRepository
    {
        public void Add(Rating rating)
        {
            using (var db = new RatingDbContext(BookingConnectionString))
            {
                db.Rating.Add(rating);
                db.SaveChanges();
            }
        }

        public IEnumerable<Rating> GetRatings(int bookingId)
        {
            using (var db = new RatingDbContext(BookingConnectionString))
            {
                var reviews = db.Rating.Where(r => r.BookingId == bookingId).ToList();
                return reviews;
            }
        }

        public void Edit(Rating rating)
        {
            using (var db = new RatingDbContext(BookingConnectionString))
            {
                Rating existingReview = db.Rating.SingleOrDefault(r => r.Id == rating.Id);

                if (existingReview != null)
                {
                    existingReview.Value = rating.Value;
                    existingReview.Details = rating.Details;
                    existingReview.Date = rating.Date;
                }
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new RatingDbContext(BookingConnectionString))
            {
                Rating rating = db.Rating.SingleOrDefault(r => r.Id == id);

                if(rating != null)
                {
                    db.Rating.Remove(rating);
                    db.SaveChanges();
                }
            }
        }

        public IEnumerable<Rating> GetUserRatings(int userId)
        {
            using (var db = new RatingDbContext(BookingConnectionString))
            {
                var reviews = db.Rating.Where(r => r.UserId == userId).ToList();
                return reviews;
            }
        }

        public double? AverageRatings(IEnumerable<RatingModel> ratings)
        {
            using (var db = new RatingDbContext(BookingConnectionString))
            {
                double? result = ratings.Average(r=>r.Value);
                return result;
            }
        }

        public Rating GetById(int id)
        {
            using(var db = new RatingDbContext(BookingConnectionString))
            {
                var review = db.Rating.SingleOrDefault(r => r.Id == id);
                return review;
            }
        }

        public IEnumerable<RatingModel> GetUnitRatings(int unitId)
        {
            using (var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                var result = from a in db.Ratings
                             join b in db.RoomBookings on a.BookingId equals b.Id
                             join c in db.Rooms on b.RoomId equals c.Id
                             where c.UnitId == unitId
                             select new RatingModel
                             {
                                 BookingId = a.BookingId,
                                 Date = a.Date,
                                 Details = a.Details,
                                 Id = a.Id,
                                 UserId = a.UserId,
                                 Value = a.Value.Value
                             };
                return result.ToList();
            }
        }

        public IEnumerable<RatingModel> GetUserRatingsExtended(int userId)
        {
            using (var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                var result = from a in db.Ratings
                             join b in db.RoomBookings on a.BookingId equals b.Id
                             join c in db.Rooms on b.RoomId equals c.Id
                             join d in db.Units on c.UnitId equals d.Id
                             where a.UserId == userId
                             select new RatingModel
                             {
                                 Id = a.Id,
                                 BookingId = a.BookingId,
                                 Date = a.Date,
                                 Details = a.Details,
                                 UserId = a.UserId,
                                 Value = a.Value.Value,
                                 Unit = d.Name
                             };
                return result.ToList();
            }
        }

        public IEnumerable<RatingModel> GetBookingRatingsExtended(int bookingId)
        {
            using (var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                var result = from a in db.Ratings
                             join b in db.RoomBookings on a.BookingId equals b.Id
                             join c in db.Rooms on b.RoomId equals c.Id
                             join d in db.Units on c.UnitId equals d.Id
                             where a.BookingId == bookingId
                             select new RatingModel
                             {
                                 Id = a.Id,
                                 BookingId = a.BookingId,
                                 Date = a.Date,
                                 Details = a.Details,
                                 UserId = a.UserId,
                                 Value = a.Value.Value,
                                 Unit = d.Name
                             };
                return result.ToList();
            }
        }
    }
}
