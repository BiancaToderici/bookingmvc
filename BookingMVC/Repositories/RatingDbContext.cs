﻿using BookingMVC.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class RatingDbContext : DbContext
    {
        public DbSet<Rating> Rating { get; set; }

        public RatingDbContext(string connectionstring) : base(connectionstring)
        {
        }
    }
}