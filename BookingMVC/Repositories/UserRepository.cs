﻿using BookingMVC.Common;
using BookingMVC.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class UserRepository : ApplicationConfiguration, IUserRepository
    {
        public User GetUser(string userName)
        {
            using (var userDbContext = new UserDbContext(BookingConnectionString))
            {
                User user = userDbContext.Users.Where(u => u.UserName == userName).SingleOrDefault();

                return user;
            }
        }

        public string GetNameById(int id)
        {
            using(var db = new UserDbContext(BookingConnectionString))
            {
                var name = db.Users.Where(u => u.Id == id).Select(u => u.UserName).SingleOrDefault();

                return name;
            }
        }

        public void Add(User user)
        {
            using( var db = new UserDbContext(BookingConnectionString))
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
        }

        public void Edit(User user)
        {
            using (var db = new UserDbContext(BookingConnectionString))
            {
                var existingUser = db.Users.SingleOrDefault(u => u.Id == user.Id);
                if (existingUser != null)
                {
                    existingUser.UserName = user.UserName;
                    existingUser.Password = user.Password;
                    existingUser.Type = user.Type;
                }

                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using( var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                var existinUser = db.Users.Where(u => u.Id == id).SingleOrDefault();
                if(existinUser != null)
                {
                    var units = db.Units.Where(u => u.UserId == id).ToList();
                    if (units.Count() > 0)
                    {
                        foreach (var unit in units)
                        {
                            var rooms = db.Rooms.Where(r => r.UnitId == unit.Id).ToList();
                            foreach (var room in rooms)
                            {
                                db.Rooms.Remove(room);
                            }
                            db.Units.Remove(unit);
                        }
                    }
                    var bookings = db.RoomBookings.Where(b => b.UserId == id).ToList();
                    var ratings = db.Ratings.Where(r => r.UserId == id).ToList();
                    foreach (var booking in bookings)
                    {
                        db.RoomBookings.Remove(booking);
                    }
                    foreach (var rating in ratings)
                    {
                        db.Ratings.Remove(rating);
                    }
                    db.Users.Remove(existinUser);
                    db.SaveChanges();
                }
            }
        }

        public User GetById(int id)
        {
            using (var db = new UserDbContext(BookingConnectionString))
            {
                var user = db.Users.SingleOrDefault(u => u.Id == id);

                return user;
            }
        }

        public IEnumerable<User> GetList()
        {
            using (var db = new UserDbContext(BookingConnectionString))
            {
                IEnumerable<User> users = db.Users.ToList();

                return users;
            }
        }
    }
}