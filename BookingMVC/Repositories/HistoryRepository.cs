﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class HistoryRepository : ApplicationConfiguration, IHistoryRepository
    {
        public void SaveHistory(History history)
        {
            using (var db = new HistoryDbContext(BookingConnectionString))
            {
                db.History.Add(history);
                db.SaveChanges();
            }
        }

        public IEnumerable<RoomBookingHistory> History(int userId)
        {
            using (var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                var result = from a in db.RoomBookings
                             join b in db.Rooms on a.RoomId equals b.Id
                             join c in db.Units on b.UnitId equals c.Id
                             where a.UserId == userId
                             select new RoomBookingHistory
                             {
                                 RoomBooking = a,
                                 Room = b,
                                 Unit = c,
                             };
                return result.ToList();
            }
        }
    }
}