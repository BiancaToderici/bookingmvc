﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace BookingMVC.Repositories
{
    public class UnitRepository : ApplicationConfiguration, IUnitRepository
    {
        /// <summary>
        /// Get List with all units
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Unit> GetList() 
        {
            using (var unitDbContext = new UnitDbContext(BookingConnectionString))
            {
                IEnumerable<Unit> units = unitDbContext.Units.ToList();

                return units;
            }
        }

        /// <summary>
        /// Search option : by name, by address
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public IEnumerable<Unit> GetUnits(string search)
        {
            using (var unitDbContext = new UnitDbContext(BookingConnectionString))
            {
                IEnumerable<Unit> unit = unitDbContext.Units.Where(u => u.Name.Contains(search) || u.Address.Contains(search)).ToList();

                return unit;
            }
        }

       /// / <summary>
       /// / Get user's offers by userId
       /// / </summary>
        //// <param name = "userId" ></ param >
        //// < returns ></ returns >
        public IEnumerable<Unit> UserOffers(int userId)
        {
            using (var unitDbContext = new UnitDbContext(BookingConnectionString))
            {
                IEnumerable<Unit> unit = unitDbContext.Units.Where(u => u.UserId == userId).ToList();

                return unit;
            }
        }

        /// <summary>
        /// Get Unit by name
        /// </summary>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public Unit GetName(string unitName)
        {
            using (var unitDbContext = new UnitDbContext(BookingConnectionString))
            {
                Unit unit = unitDbContext.Units.Where(u => u.Name == unitName).SingleOrDefault();
                return unit;
            }
        }

        public string GetNameById(int id)
        {
            using (var unitDbContext = new UnitDbContext(BookingConnectionString))
            {
                var unit = unitDbContext.Units.Where(u => u.Id == id).Select(u => u.Name).Single();
                return unit;
            }
        }

        public Unit GetById(int unitId)
        {
            using (var db = new UnitDbContext(BookingConnectionString))
            {
                Unit unit = db.Units.Where(u => u.Id == unitId).SingleOrDefault();
                return unit;
            }
        }

        public void Add(Unit unit)
        {
            using (var db = new UnitDbContext(BookingConnectionString))
            {
                db.Units.Add(unit);
                db.SaveChanges();
            }
        }

        public void Edit(Unit unit)
        {
            using (var db = new UnitDbContext(BookingConnectionString))
            {
                Unit existingUnit = db.Units.Where(u => u.Id == unit.Id).SingleOrDefault();

                if (existingUnit != null)
                {
                    existingUnit.Name = unit.Name;
                    existingUnit.Type = unit.Type;
                    existingUnit.Stars = unit.Stars;
                    existingUnit.Address = unit.Address;
                    existingUnit.Contact = unit.Contact;
                    existingUnit.Description = unit.Description;
                }
                db.SaveChanges();
            }
        }
        public void DeleteUnit(int id)
        {
            using (var db = new DbEntitiesDbContext(BookingConnectionString))
            {
                var rooms = db.Rooms.Where(r => r.UnitId == id);
                var unit = db.Units.Where(u => u.Id == id).SingleOrDefault();
                if (rooms != null)
                {
                    foreach (var room in rooms)
                    {
                        db.Rooms.Remove(room);
                    }
                    
                    db.Units.Remove(unit);
                    
                    db.SaveChanges();
                }
            }
        }
    }
}