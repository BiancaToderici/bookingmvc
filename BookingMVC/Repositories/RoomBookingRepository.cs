﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class RoomBookingRepository : ApplicationConfiguration, IRoomBookingRepository
    {
        public IEnumerable<RoomBooking> GetRoomsBooked()
        {
            using (var roomsbookedDbContext = new RoomBookingDbContext(BookingConnectionString))
            {
                IEnumerable<RoomBooking> roomsbooked = roomsbookedDbContext.RoomsBooking.ToList();

                return roomsbooked;
            }
        }

        public RoomBooking BookRoom(RoomBookingModel roomBookingModel)
        {
            using ( var db = new RoomBookingDbContext(BookingConnectionString))
            {
                RoomBooking roomBooked = new RoomBooking();
                roomBooked.DateFrom = roomBookingModel.DateFrom;
                roomBooked.DateTo = roomBookingModel.DateTo;
                roomBooked.MealType = roomBookingModel.MealType;
                roomBooked.RoomId = roomBookingModel.RoomId;
                roomBooked.UserId = roomBookingModel.UserId;

                db.RoomsBooking.Add(roomBooked);
                db.SaveChanges();

            return roomBooked;
            }
        }
    }
}