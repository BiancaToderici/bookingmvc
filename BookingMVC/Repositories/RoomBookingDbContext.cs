﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingMVC.Repositories
{
    public class RoomBookingDbContext : DbContext
    {
        public DbSet<RoomBooking> RoomsBooking { get; set; }

        public RoomBookingDbContext(string connectionstring) : base(connectionstring)
        {
        }
    }
}