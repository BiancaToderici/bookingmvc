﻿using BookingMVC.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace BookingMVC.Repositories
{
    public class RoomBooking
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage ="Select a date for check-in")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFrom { get; set; }
        [Required(ErrorMessage ="Select a date for check-out")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateTo { get; set; }
        [Range(1,int.MaxValue,ErrorMessage ="Select a meal type")]
        public MealType MealType { get; set; }
        public int RoomId { get; set; }
        public int UserId { get; set; }
    }
}