﻿namespace BookingMVC.Common
{
    public enum UnitType
    {
        Hotel = 1,
        Pension = 2,
        House = 3
    }
}