﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public enum MealType
    {
        None = 1,
        Breakfast = 2,
        BreakfastandDinner = 3,
        All = 4
    }
}