﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public class Photo
    {
        public int Id { get; set; }
        public PhotoType Type { get; set; }
        public byte[] Photos { get; set; }
        public int SubjectId { get; set; }
    }
}