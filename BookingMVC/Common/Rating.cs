﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingMVC.Common
{
    public class Rating
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Rate this unit")]
        [Column("Rating")]
        public int? Value { get; set; }
        public string Details { get; set; }
        public DateTime Date { get; set; }
        public int BookingId { get; set; }
        public int UserId { get; set; }
    }
}