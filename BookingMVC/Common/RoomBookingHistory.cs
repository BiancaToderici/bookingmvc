﻿using BookingMVC.Repositories;

namespace BookingMVC.Common
{
    public class RoomBookingHistory
    {
        public RoomBooking RoomBooking { get; set; }
        public Room Room { get; set; }
        public Unit Unit { get; set; }
    }
}