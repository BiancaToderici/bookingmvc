﻿using BookingMVC.Common;
using System.ComponentModel.DataAnnotations;

namespace BookingMVC.Repositories
{
    public class Unit
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter a name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Select a valid type")]
        public UnitType Type { get; set; }
        public int Stars { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage ="Enter an address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Enter a phone number")]
        public string Contact { get; set; }
        public int? UserId { get; set; }
    }
}