﻿using System;
using System.Configuration;

namespace BookingMVC.Common
{
    public class ApplicationConfiguration
    {
        private static string bookingConnectionString;
        private static string loggerConnectionString;

        public static string BookingConnectionString
        {
            get
            {
                return GetConnectionString(ConnectionType.Booking);
            }
        }

        public static string LoggerConnectionString
        {
            get
            {
                return GetConnectionString(ConnectionType.Logger);
            }
        }

        public static string FeedbackUrl
        {
            get
            {
                return GetFeedbackURL();
            }
        }

        private static string GetFeedbackURL()
        {
            var feedbackUrl = ConfigurationManager.AppSettings["FeedbackURL"];
            return feedbackUrl;
        }

        private static string GetConnectionString(ConnectionType connectionType)
        {
            var booking = "BookingConnectionString";
            var logger = "LoggerConnectionString";
            try
            {
                switch (connectionType)
                {
                    case ConnectionType.Booking:
                        return bookingConnectionString = ConfigurationManager.ConnectionStrings[booking].ConnectionString;
                    case ConnectionType.Logger:
                        return loggerConnectionString = ConfigurationManager.ConnectionStrings[logger].ConnectionString;
                    default:
                        return "";
                }
            }
            catch (Exception e)
            {
                throw new ApplicationException($"Unable to get DB Connection string from Web.Config {e.Message}");
            }
        }
    }
}