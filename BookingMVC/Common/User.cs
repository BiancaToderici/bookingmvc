﻿using BookingMVC.Common;
using System.ComponentModel.DataAnnotations;

namespace BookingMVC.Repositories
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please select a valid type")]
        public UserType Type { get; set; }
    }
}