﻿namespace BookingMVC.Common
{
    public enum UserType
    {
        Guest = 1,
        RegularUser = 2,
        Administrator = 3
    }
}