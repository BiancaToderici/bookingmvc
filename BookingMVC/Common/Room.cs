﻿using System.ComponentModel.DataAnnotations;

namespace BookingMVC.Repositories
{
    public class Room
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter a Room Number")]
        public int RoomNr { get; set; }
        [Required(ErrorMessage = "Enter a type")]
        public string Type { get; set; }
        public int NrPersons { get; set; }
        public string Details { get; set; }
        [Required(ErrorMessage = "Please enter a price")]
        public int Price { get; set; }
        public int UnitId { get; set; }
        
    }
}