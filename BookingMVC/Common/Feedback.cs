﻿namespace BookingMVC.Common
{
    public class Feedback
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DateTime { get; set; }
        public int UserId { get; set; }
    }
}