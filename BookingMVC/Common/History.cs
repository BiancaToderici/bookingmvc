﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public class History
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoomId { get; set; }
    }
}