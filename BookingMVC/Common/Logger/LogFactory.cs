﻿using BookingMVC.Common.Logger;
using BookingMVC.Repositories.Contracts;
using System;

namespace BookingMVC.Common
{
    public class LogFactory : ILogFactory
    {
        private readonly ILogRepository _logRepository;
        public LogFactory(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }
        public ILogBase GetLogger(LoggerType type)
        {
            ILogBase logger = null;
            
            switch (type)
            {
                case LoggerType.File:
                    logger = new FileLogger();
                    break;
                case LoggerType.Database:
                    logger = new DbLogger(_logRepository);
                    break;
                default:
                    throw new Exception($"Unknown type: {type}");
            }
            return logger;
        }
    }
}