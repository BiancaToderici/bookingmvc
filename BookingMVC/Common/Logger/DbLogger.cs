﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public class DbLogger : ILogBase
    {
        private ILogRepository _logRepository;
        public DbLogger(ILogRepository logRepository)
        {
            _logRepository = logRepository ;
        }
        public void Debug(string message)
        {
            Log log = new Log()
            {
                Message = message,
                Timestamp = DateTime.Now,
                Type = (int)LogType.Debug
            };
            _logRepository.Save(log);
        }

        public void Error(string message, string stacktrace)
        {
            Log log = new Log()
            {
                Message = message,
                Timestamp = DateTime.Now,
                Type = (int)LogType.Error,
                StackTrace = stacktrace
            };
            _logRepository.Save(log);
        }

        public void Information(string message)
        {
            Log log = new Log()
            {
                Message = message,
                Timestamp = DateTime.Now,
                Type = (int)LogType.Error
            };
            _logRepository.Save(log);
        }
    }
}