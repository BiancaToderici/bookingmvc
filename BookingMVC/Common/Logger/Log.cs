﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public class Log
    {
        [Key]
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public int Type { get; set; }
        public string StackTrace { get; set; }
    }
}