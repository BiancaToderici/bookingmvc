﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Common.Logger
{
    public interface ILogFactory
    {
        ILogBase GetLogger(LoggerType type);
    }
}
