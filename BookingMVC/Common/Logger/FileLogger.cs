﻿using System;
using System.IO;

namespace BookingMVC.Common
{
    public class FileLogger : ILogBase
    {
        private string _path;
        private DateTime _dateTime;
        
        public FileLogger()
        {
            _path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "Logs\\logger.txt");
            _dateTime = DateTime.Now;
        }
        public void Debug(string message)
        {
            Log(message, "Debug");
        } 

        public void Error(string message, string stackTrace)
        {
            Log(message, "Error", stackTrace);
        }

        public void Information(string message)
        {
             Log(message, "Information");
        }

        private void Log(string message, string type, string stacktrace = null)
        {
            using (StreamWriter streamWriter = File.AppendText(_path))
            {
                streamWriter.WriteLine($"{_dateTime} - {type} : {message} - StackTrace: {stacktrace}");
            }
            
        }
    }
}