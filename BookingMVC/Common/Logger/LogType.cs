﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public enum LogType
    {
        Info = 1,
        Error = 2,
        Debug = 3
    }
}