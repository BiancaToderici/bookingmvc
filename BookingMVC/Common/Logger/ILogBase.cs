﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Common
{
    public interface ILogBase
    {
        void Information(string message);
        void Error(string message, string stacktrace = null);
        void Debug(string message);
    }
}