﻿using BookingMVC.Common;
using BookingMVC.Common.Logger;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVC.Services.ContractsServices;
using Ninject.Modules;
using Ninject.Web.WebApi.Filter;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Validation;

namespace BookingMVC.App_Start
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IHistoryService>().To<HistoryService>();
            Bind<IPhotoService>().To<PhotoService>();
            Bind<IRoomBookingService>().To<RoomBookingService>();
            Bind<IRoomService>().To<RoomService>();
            Bind<IUnitService>().To<UnitService>();
            Bind<IUserService>().To<UserService>();
            Bind<IHistoryRepository>().To<HistoryRepository>();
            Bind<IPhotoRepository>().To<PhotoRepository>();
            Bind<IRoomBookingRepository>().To<RoomBookingRepository>();
            Bind<IRoomRepository>().To<RoomRepository>();
            Bind<IUnitRepository>().To<UnitRepository>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IRatingRepository>().To<RatingRepository>();
            Bind<IRatingService>().To<RatingService>();
            Bind<ILogFactory>().To<LogFactory>();
            Bind<ILogRepository>().To<LogRepository>();
            Bind<IFeedbackService>().To<FeedbackService>();
            Bind<DefaultFilterProviders>().ToConstant(new DefaultFilterProviders(GlobalConfiguration.Configuration.Services.GetFilterProviders()));
            Bind<DefaultModelValidatorProviders>().ToConstant(new DefaultModelValidatorProviders(GlobalConfiguration.Configuration.Services.GetServices(typeof(ModelValidatorProvider)).Cast<ModelValidatorProvider>()));
        }
    }
}