﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BookingMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
               name: "Book",
               url: "room/book/{roomId}",
               defaults: new { controller = "Room", action = "Book"});

            routes.MapRoute(
               name: "Offers",
               url: "home/offers/{userId}",
               defaults: new { controller = "Home", action = "Offers" });

            routes.MapRoute(
                name: "Show",
                url: "history/show/{userId}",
                defaults: new { controller = "History", action = "Show" });

            routes.MapRoute(
                name: "Picture",
                url: "home/picture/{subjectId}",
                defaults: new { controller = "Home", action = "Picture" });

            routes.MapRoute(
                name: "RoomPicture",
                url: "room/picture/{subjectId}",
                defaults: new { controller = "Room", action = "Picture" });

            routes.MapRoute(
                name: "Edit",
                url: "room/edit/{roomId}",
                defaults: new { controller = "Room", action = "Edit" });

            routes.MapRoute(
                name: "EditUnit",
                url: "unit/edit/{unitId}",
                defaults: new { controller = "Unit", action = "Edit" });

            routes.MapRoute(
               name: "AddPhotoToUnit",
               url: "unit/addphoto/{unitId}",
               defaults: new { controller = "Unit", action = "AddPhoto" });

            routes.MapRoute(
               name: "AddPhotoToRoom",
               url: "room/addphoto/{unitId}",
               defaults: new { controller = "Room", action = "AddPhoto" });

            routes.MapRoute(
               name: "AddRoomToUnit",
               url: "unit/addroom/{unitId}",
               defaults: new { controller = "Unit", action = "AddRoom" });

            routes.MapRoute(
               name: "MyReviews",
               url: "unit/myreviews/{userId}",
               defaults: new { controller = "Unit", action = "MyReviews" });

            routes.MapRoute(
               name: "AddReview",
               url: "unit/addreview/{bookingId}",
               defaults: new { controller = "Unit", action = "AddReview" });

            routes.MapRoute(
               name: "EditReview",
               url: "unit/editreview/{id}",
               defaults: new { controller = "Unit", action = "EditReview" });

            routes.MapRoute(
               name: "EditUser",
               url: "user/edit/{userId}",
               defaults: new { controller = "User", action = "Edit" });

            routes.MapRoute(
               name: "CreateFeedback",
               url: "feedback/create/{userId}/{userName}",
               defaults: new { controller = "Feedback", action = "Create" });

            routes.MapRoute(
               name: "EditFeedback",
               url: "feedback/edit/{id}",
             defaults: new { controller = "Feedback", action = "Edit" });

            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
           );
        }
    }
}
