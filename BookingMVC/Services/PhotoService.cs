﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Web;

namespace BookingMVC.Services
{
    public class PhotoService : IPhotoService
    {
        IPhotoRepository _photoRepository;
        public PhotoService(IPhotoRepository photoRepository)
        {
            _photoRepository = photoRepository;
        }

        public IEnumerable<PhotosModel> DisplayPhoto(int subjectId,PhotoType photoType)
        {
            List<PhotosModel> imageViews = new List<PhotosModel>();
            var photos = _photoRepository.GetPhotos(subjectId, photoType);
            foreach (var item in photos)
            {
                PhotosModel imageViewModel = new PhotosModel()
                {
                    Photo = item.Photos
                };
                imageViews.Add(imageViewModel);
            }
            return imageViews;
        }

        public void AddPhoto(HttpPostedFileBase file,PhotoType photoType, int subjectId)
        {
            try
            {
                Byte[] upload = new Byte[file.ContentLength];
                file.InputStream.Read(upload, 0, file.ContentLength);

                Photo photo = new Photo()
                {
                    Photos = upload,
                    SubjectId = subjectId,
                    Type = photoType
                };
                _photoRepository.AddPhoto(photo);
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message);
            }
        }
    }
}