﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Services.ContractsServices;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BookingMVC.Services
{
    public class FeedbackService : ApplicationConfiguration, IFeedbackService
    {
        public async Task<IEnumerable<FeedbackModel>> Get()
        {
            var client = new HttpClient();
            var response = await client.GetStringAsync(FeedbackUrl).ConfigureAwait(false);
            var feedbacks = JsonConvert.DeserializeObject<IEnumerable<FeedbackModel>>(response);
            return feedbacks;
        }

        public async Task<HttpResponseMessage> Create(FeedbackModel feedbackModel)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var data = JsonConvert.SerializeObject(feedbackModel);
            var response = await client.PostAsJsonAsync(FeedbackUrl, data).ConfigureAwait(false);
            return response;
        }

        public async Task<FeedbackModel> GetById(string id)
        {
            var client = new HttpClient();
            var str = await client.GetStringAsync($"{FeedbackUrl}/{id}").ConfigureAwait(false);
            var feedback = JsonConvert.DeserializeObject<Feedback>(str);
            FeedbackModel feedbackModel = new FeedbackModel
            {
                Description = feedback.Description,
                Id = feedback.Id,
                DateTime = feedback.DateTime,
                Name = feedback.Name,
                UserId = feedback.UserId
            };
            return feedbackModel;
        }

        public async Task<HttpResponseMessage> Edit(Feedback feedback)
        {
            var feedbackModel = new FeedbackModel
            {
                Description = feedback.Description,
                Name = feedback.Name,
                UserId = feedback.UserId,
                Id = feedback.Id
            };
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var data = JsonConvert.SerializeObject(feedbackModel);
            var response = await client.PutAsJsonAsync($"{FeedbackUrl}/{feedback.Id}", data).ConfigureAwait(false);
            return response;
        }

        public async Task Delete(string id)
        {
            var client = new HttpClient();
            var response = await client.DeleteAsync($"{FeedbackUrl}/{id}").ConfigureAwait(false);
        }

        public async Task<IEnumerable<FeedbackModel>> Search(string search)
        {
            var client = new HttpClient();
            var str = await client.GetStringAsync($"{FeedbackUrl}/Search/{search}").ConfigureAwait(false);
            var feedbacks = JsonConvert.DeserializeObject<IEnumerable<string>>(str);
            var feedbacksModels = new List<FeedbackModel>();
            foreach (var item in feedbacks)
            {
                FeedbackModel feedbackModel = new FeedbackModel
                {
                    Name = item
                };
                feedbacksModels.Add(feedbackModel);
            }
            return feedbacksModels;
        }

        public async Task<IEnumerable<FeedbackModel>> GetByUserName(string userName)
        {
            var client = new HttpClient();
            var str = await client.GetStringAsync($"{FeedbackUrl}/GetByUsername/{userName}").ConfigureAwait(false);
            var feedbacks = JsonConvert.DeserializeObject<IEnumerable<FeedbackModel>>(str);
            return feedbacks;
        }
    }
}