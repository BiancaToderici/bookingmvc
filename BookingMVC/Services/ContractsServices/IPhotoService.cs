﻿using BookingMVC.Common;
using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BookingMVC.Services.ContractsServices
{
     public interface IPhotoService
    {
        IEnumerable<PhotosModel> DisplayPhoto(int subjectId, PhotoType photoType);
        void AddPhoto(HttpPostedFileBase file, PhotoType photoType, int subjectId);
    }

}
