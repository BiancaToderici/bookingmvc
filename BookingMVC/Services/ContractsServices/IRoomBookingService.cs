﻿using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
    public interface IRoomBookingService
    {
        void BookARoom(RoomBookingModel roomBookingModel);
    }
}
