﻿using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
    public interface IHistoryService
    {
        void RoomHistory(HistoryModel historyModel);
        IEnumerable<HistoryRoomBookingModel> RoomsBooked(int userId);
    }
}
