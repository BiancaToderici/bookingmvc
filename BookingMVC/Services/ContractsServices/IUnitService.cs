﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
    public interface IUnitService
    {
        IEnumerable<Unit> GetUnits();
        IEnumerable<Unit> GetFreeUnits(string search, DateTime? DateFrom, DateTime? DateTo);
        IEnumerable<Unit> SearchList(string search);
        IEnumerable<Unit> UserOffers(int userId);
        string GetNameById(int id);
        Unit UnitById(int unitId);
        bool UnitExist(string userName);
        void Create(UnitModel unitModel);
        void Edit(UnitModel unitModel);
        void Delete(int unitId);
    }
}
