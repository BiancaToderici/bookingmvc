﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
    public interface IUserService
    {
        bool IsAuthorized(ref User user);
        bool IsAdmin(string userName);
        bool IsRegular(string userName);
        User GetUser(string userName);
        string GetNameById(int id);
        void Add(UserModel userModel);
        bool UserExists(string userName);
        IEnumerable<User> GetList();
        void Edit(UserModel userModel);
        void Delete(int userId);
        User GetById(int userId);
    }
}
