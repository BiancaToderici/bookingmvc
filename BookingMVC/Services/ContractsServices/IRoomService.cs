﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
        public interface IRoomService
        {
            IEnumerable<Room> GetRooms(int id);
            Room GetById(int roomId);
            bool RoomNrExists(int roomNr, int unitId);
            int GetRoomNr(int roomId);
            void AddRoom(RoomModel roomModel);
            void Edit(RoomModel roomModel, int roomId);
            IEnumerable<Room> GetFreeRooms(DateTime? dateFrom, DateTime? dateTo, int? unitId);
            void Delete(int id);
        }
}
