﻿using BookingMVC.Common;
using BookingMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
    public interface IRatingService
    {
        void Add(RatingModel ratingModel);
        void Edit(RatingModel ratingModel);
        IEnumerable<RatingModel> GetByUser(int userId);
        double? GetAverage(int unitId);
        Rating GetById(int id);
        void Delete(int ratingId);
        IEnumerable<RatingModel> GetByUnit(int unitId);
        IEnumerable<RatingModel> GetByBooking(int bookingId);
    }
}
