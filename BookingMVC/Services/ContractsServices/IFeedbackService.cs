﻿using BookingMVC.Common;
using BookingMVC.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BookingMVC.Services.ContractsServices
{
    public interface IFeedbackService
    {
        Task<IEnumerable<FeedbackModel>> Get();
        Task<HttpResponseMessage> Create(FeedbackModel feedbackModel);
        Task<HttpResponseMessage> Edit(Feedback feedback);
        Task<FeedbackModel> GetById(string id);
        Task<IEnumerable<FeedbackModel>> GetByUserName(string userName);
        Task<IEnumerable<FeedbackModel>> Search(string search);
        Task Delete(string id);
    }
}
