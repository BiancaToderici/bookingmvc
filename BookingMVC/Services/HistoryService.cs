﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Services
{
    public class HistoryService : IHistoryService
    {
        IHistoryRepository _historyRepository;
        IRatingRepository _ratingRepository;
        public HistoryService(IHistoryRepository context,IRatingRepository ratingRepository)
        {
            _historyRepository = context;
            _ratingRepository = ratingRepository;
        }
/// <summary>
/// Add room booked by user to the history DB
/// </summary>
/// <param name="historyModel"></param>
        public void RoomHistory(HistoryModel historyModel)
        {
            History history = new History
            {
                RoomId = historyModel.RoomId,
                UserId = historyModel.UserId
            };
            _historyRepository.SaveHistory(history);
        }

        public IEnumerable<HistoryRoomBookingModel> RoomsBooked(int userId)
        {
            List<HistoryRoomBookingModel> bookingModels = new List<HistoryRoomBookingModel>();
            var bookings = _historyRepository.History(userId);
           
            foreach (var item in bookings)
            {
                var ratings = _ratingRepository.GetRatings(item.RoomBooking.Id);
                bool hasRating = ratings.Any();

                HistoryRoomBookingModel roomBookingModel = new HistoryRoomBookingModel
                {
                    DateFrom = item.RoomBooking.DateFrom,
                    DateTo = item.RoomBooking.DateTo,
                    RoomNr = item.Room.RoomNr,
                    Price = item.Room.Price,
                    Name = item.Unit.Name,
                    Address = item.Unit.Address,
                    BookingId = item.RoomBooking.Id,
                    HasRating = hasRating
                };
                bookingModels.Add(roomBookingModel);
            }
            return bookingModels;
        }
    }
}