﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BookingMVC.Services
{
    public class UserService : IUserService
    {
        IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool IsAuthorized(ref User user)
        {
            bool isAuthorized;
            var actualUser = GetUser(user.UserName);
            isAuthorized = IsAuthorized(user.UserName, user.Password, actualUser);
         
            if(actualUser != null)
            {
                user = actualUser;
            }

            return isAuthorized;
        }

        private bool IsAuthorized(string userName, string password, User user)
        {
            bool isAutorized;

            if (user != null)
            { 
                if (user.Password.Equals(password))
                {
                    isAutorized = true;
                }
                else
                {
                    isAutorized = false;
                }
            }
            else
            {
                isAutorized = false;
            }
            return isAutorized;
        }

        public bool IsAdmin(string userName)
        {
            bool isadmin;
            var user = _userRepository.GetUser(userName);

            if(user!=null)
            {
                if(user.Type.Equals(UserType.Administrator))
                {
                    isadmin = true;
                }
                else
                {
                    isadmin = false;
                }
            }
            else
            {
                isadmin = false;
            }
            return isadmin;
        }

        public bool IsRegular(string userName)
        {
            bool isregular;
            var user = _userRepository.GetUser(userName);

            if (user != null)
            {
                if (user.Type.Equals(UserType.RegularUser))
                {
                    isregular = true;
                }
                else
                {
                    isregular = false;
                }
            }
            else
            {
                isregular = false;
            }
            return isregular;

        }

        public User GetUser(string userName)
        {
            var user = _userRepository.GetUser(userName);
            return user;
        }

        public string GetNameById(int id)
        {
            var name = _userRepository.GetNameById(id);
            return name;
        }

        public void Add(UserModel userModel)
        {
            User user = new User
            {
                UserName = userModel.UserName,
                Password = userModel.Password,
                Type = userModel.Type
            };
            _userRepository.Add(user);
        }

        public bool UserExists(string userName)
        {
            var user = _userRepository.GetUser(userName);
            var exists = user != null ? true : false;

            return exists;
        }

        public IEnumerable<User> GetList()
        {
            var users = _userRepository.GetList();
            return users;
        }

        public void Edit(UserModel userModel)
        {
            User user = new User
            {
                Id = userModel.Id,
                UserName = userModel.UserName,
                Password = userModel.Password,
                Type = userModel.Type
            };

            _userRepository.Edit(user);
        }

        public void Delete(int userId)
        {
            _userRepository.Delete(userId);
        }

        public User GetById(int userId)
        {
            var user = _userRepository.GetById(userId);
            return user;
        }
    }
}