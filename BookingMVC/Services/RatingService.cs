﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Services
{
    public class RatingService : IRatingService
    {
        IRatingRepository _ratingRepository;
        public RatingService(IRatingRepository ratingRepository)
        {
            _ratingRepository = ratingRepository;
        }

        public void Add(RatingModel ratingModel)
        {
            Rating rating = new Rating
            {
                Date = DateTime.Now,
                Details = ratingModel.Details,
                BookingId =  ratingModel.BookingId,
                UserId = ratingModel.UserId,
                Value = ratingModel.Value
            };

            _ratingRepository.Add(rating);
        }

        public void Edit(RatingModel ratingModel)
        {
            Rating rating = new Rating
            {
                Id = ratingModel.Id,
                Date = DateTime.Now,
                Details = ratingModel.Details,
                BookingId = ratingModel.BookingId,
                UserId = ratingModel.UserId,
                Value = ratingModel.Value
            };

            _ratingRepository.Edit(rating);
        }

        public IEnumerable<RatingModel> GetByUser(int userId)
        {
            var reviews = _ratingRepository.GetUserRatingsExtended(userId);
            return reviews;
        }

        public double? GetAverage(int unitId)
        {
            var ratings = _ratingRepository.GetUnitRatings(unitId);
            var result = _ratingRepository.AverageRatings(ratings);
            return result;
        }

        public void Delete(int ratingId)
        {
            _ratingRepository.Delete(ratingId);
        }

        public Rating GetById(int id)
        {
            var review = _ratingRepository.GetById(id);
            return review;
        }

        public IEnumerable<RatingModel> GetByUnit(int unitId)
        {
            var ratings = _ratingRepository.GetUnitRatings(unitId);
            return ratings;
        }

        public IEnumerable<RatingModel> GetByBooking(int bookingId)
        {
            var ratings = _ratingRepository.GetBookingRatingsExtended(bookingId);
            return ratings;
        }
    }
}