﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingMVC.Services
{
    public class UnitService : IUnitService
    {
        IUnitRepository _unitRepository;
        IRoomRepository _roomRepository;
        public UnitService(IUnitRepository repository, IRoomRepository roomRepository)
        {
            _unitRepository = repository;
            _roomRepository = roomRepository;
        }

        public IEnumerable<Unit> GetUnits()
        {
            var units = _unitRepository.GetList();
            return units;
        }

        public IEnumerable<Unit> GetFreeUnits(string search, DateTime? DateFrom, DateTime? DateTo)
        {
            IList<Room> freeRooms = new List<Room>();
            IList<Unit> freeUnits = new List<Unit>();
            var units = _unitRepository.GetUnits(search);
            foreach (var unit in units)
            {
                var rooms = _roomRepository.GetRooms(unit.Id);
                foreach (var room in rooms)
                {
                    bool isbooked = _roomRepository.IsBooked(room, DateFrom, DateTo);
                    if (!isbooked)
                    {
                        freeRooms.Add(room);
                    }
                }
                if (freeRooms.Count(f => f.UnitId == unit.Id) > 0)
                {
                    freeUnits.Add(unit);
                }
            }
            return freeUnits;
        }

        public IEnumerable<Unit> SearchList(string search)
        {
            var unit = _unitRepository.GetUnits(search);
            return unit;
        }

        public IEnumerable<Unit> UserOffers(int userId)
        {
            IEnumerable<Unit> units = _unitRepository.UserOffers(userId);
            return units;
        }

        public string GetNameById(int id)
        {
            var unit = _unitRepository.GetNameById(id);
            return unit;
        }

        public Unit UnitById(int unitId)
        {
            var unit = _unitRepository.GetById(unitId);
            return unit;
        }

        public bool UnitExist(string userName)
        {
            bool exist;
            var unit = _unitRepository.GetName(userName);

            if (unit != null)
            {
                exist = true;
            }
            else
            {
                exist = false;
            }
            return exist;
        }

        public void Create(UnitModel unitModel)
        {
            Unit unit = new Unit
            {
                Name = unitModel.Unit.Name,
                Type = unitModel.Unit.Type,
                Stars = unitModel.Unit.Stars,
                Address = unitModel.Unit.Address,
                Contact = unitModel.Unit.Contact,
                Description = unitModel.Unit.Description,
                UserId = unitModel.Unit.UserId
            };
            _unitRepository.Add(unit);
        }

        public void Edit(UnitModel unitModel)
        {
            _unitRepository.Edit(unitModel.Unit);
        }

        public void Delete(int unitId)
        {
            _unitRepository.DeleteUnit(unitId);
        }
    }
}