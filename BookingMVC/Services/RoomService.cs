﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;

namespace BookingMVC.Services
{
    public class RoomService : IRoomService
    {
        IRoomRepository _roomRespository;
        public RoomService(IRoomRepository roomRepository)
        {
            _roomRespository = roomRepository;
        }

        public IEnumerable<Room> GetRooms(int id)
        {
            var rooms = _roomRespository.GetRooms(id);
            return rooms;
        }

        public Room GetById(int roomId)
        {
            var room = _roomRespository.GetById(roomId);
            return room;
        }

        public bool RoomNrExists(int roomNr,int unitId)
        {
            bool exist;

            var room = _roomRespository.GetNr(roomNr,unitId);
            if(room != null)
            {
                exist = true;
            }
            else
            {
                exist = false;
            }
            return exist;
        }

        public int GetRoomNr(int roomId)
        {
            var room = _roomRespository.RoomNr(roomId);
            return room;
        }

        public void AddRoom(RoomModel roomModel)
        {
            _roomRespository.AddRoom(roomModel);
        }

        public void Edit(RoomModel roomModel, int roomId)
        {
            Room room = new Room
            {
                Id = roomId,
                Details = roomModel.Details,
                NrPersons = roomModel.NrPersons,
                Price = roomModel.Price,
                RoomNr = roomModel.RoomNr,
                Type = roomModel.Type,
            };

            _roomRespository.Edit(room);
        }

        public IEnumerable<Room> GetFreeRooms(DateTime? dateFrom, DateTime? dateTo,int? unitId)
        {
            var freeRooms = _roomRespository.GetFreeRooms(dateFrom, dateTo,unitId);
            return freeRooms;
        }

        public void Delete(int id)
        {
            _roomRespository.Delete(id);
        }
    }
}