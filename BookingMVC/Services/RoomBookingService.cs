﻿using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services.ContractsServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Services
{
    public class RoomBookingService : IRoomBookingService
    {
        IRoomBookingRepository _roomBookingRepository;
        public RoomBookingService(IRoomBookingRepository roomBookingRepository)
        {
            _roomBookingRepository = roomBookingRepository;
        }

        public void BookARoom(RoomBookingModel roomBookingModel)
        {
            _roomBookingRepository.BookRoom(roomBookingModel);
        }
    }
}