﻿using System.Web;

namespace BookingMVC.Models
{
    public class PhotosModel
    {
        public byte[] Photo { get; set; }
    }
}