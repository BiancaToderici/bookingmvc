﻿using BookingMVC.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class HistoryModel
    {
        public int RoomId { get; set; }
        public int UserId { get; set; }
    }
}