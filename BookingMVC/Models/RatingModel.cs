﻿using BookingMVC.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class RatingModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public int? Value { get; set; }
        public string Details { get; set; }
        public int BookingId { get; set; }
        public int UserId { get; set; }
        public string Unit { get; set; }
    }
}