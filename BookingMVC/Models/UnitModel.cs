﻿using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class UnitModel
    {
        public Unit Unit { get; set; }
        public string Rating { get; set; }
    }
}