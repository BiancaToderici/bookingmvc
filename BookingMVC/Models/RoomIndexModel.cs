﻿using BookingMVC.Common;
using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class RoomIndexModel
    {
        public IEnumerable<Room> Rooms { get; set; }
        public string UnitName { get; set; }
        public IEnumerable<PhotosModel> Photos { get; set; }
        public IEnumerable<RatingModel> Ratings { get; set; }
        public string Average { get; set; }
    }
}