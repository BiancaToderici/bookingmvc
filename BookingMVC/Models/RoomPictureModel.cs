﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class RoomPictureModel
    {
        public IEnumerable<PhotosModel> Photos { get; set; }
        public int RoomNr { get; set; }
    }
}