﻿using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Unit = BookingMVC.Repositories.Unit;

namespace BookingMVC.Models
{
    public class UnitIndexModel
    {
        public IEnumerable<UnitModel> Units { get; set; }
    }
}