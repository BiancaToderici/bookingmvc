﻿namespace BookingMVC.Models
{
    public class FeedbackModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DateTime { get; set; }
        public int UserId { get; set; }
    }
}