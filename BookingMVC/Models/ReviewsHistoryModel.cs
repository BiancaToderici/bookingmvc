﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class ReviewsHistoryModel
    {
        public IEnumerable<RatingModel> Ratings { get; set; }
    }
}