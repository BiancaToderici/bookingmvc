﻿using BookingMVC.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookingMVC.Models
{
    public class RoomModel
    {
        [Required(ErrorMessage = "Please enter a Room Number")]
        public int RoomNr { get; set; }
        [Required(ErrorMessage = "Enter a type")]
        public string Type { get; set; }
        [Required(ErrorMessage = "Enter number of persons")]
        public int NrPersons { get; set; }
        public string Details { get; set; }
        [Required(ErrorMessage = "Please enter a price")]
        public int Price { get; set; }
        public int UnitId { get; set; }
    }
}