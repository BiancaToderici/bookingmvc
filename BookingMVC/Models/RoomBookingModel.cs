﻿using BookingMVC.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace BookingMVC.Models
{
    public class RoomBookingModel
    {
        [Required(ErrorMessage = "Please select check-in date")]
        public DateTime DateFrom { get; set; }
        [Required(ErrorMessage = "Please select check-out date")]
        public DateTime DateTo { get; set; }
        [Required(ErrorMessage = "Select a valid meal type")]
        public MealType MealType { get; set; }
        public int RoomId { get; set; }
        public int UserId { get; set; }
    }
}