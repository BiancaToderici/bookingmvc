﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookingMVC.Models
{
    public class HistoryRoomBookingModel
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFrom { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateTo { get; set; }
        public int RoomNr { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int UnitId { get; set; }
        public int BookingId { get; set; }
        public bool HasRating { get; set; }
    }
}