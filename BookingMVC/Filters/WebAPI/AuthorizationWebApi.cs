﻿using BookingMVC.Common;
using BookingMVC.Repositories;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BookingMVC.Filters
{
    public class AuthorizationWebAPI : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            User user = (User)HttpContext.Current.Session["User"];

            if (user.Type != UserType.Administrator)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Access denied");
            }
            base.OnActionExecuting(actionContext);
        }
    }
}