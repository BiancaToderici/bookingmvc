﻿using BookingMVC.Common;
using BookingMVC.Repositories;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BookingMVC.Filters
{
    public class Authorization : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            User user = (User)HttpContext.Current.Session["User"];

            if (user.Type != UserType.Administrator)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    {"Controller","Home"},
                    {"Action","Index" }
                });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}