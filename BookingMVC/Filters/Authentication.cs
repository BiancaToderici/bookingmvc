﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BookingMVC.Filters
{
    public class Authentication : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["User"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    {"Controller","Login"},
                    {"Action","Login" }
                });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}