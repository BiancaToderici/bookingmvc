﻿using BookingMVC.Common;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BookingMVCTests
{
    [TestClass]
    public class PhotoServiceUnitTest
    {
        private Mock<IPhotoRepository> _mockPhotoRepository;
        private PhotoService _photoService;
        const int subjectId = 1;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockPhotoRepository = new Mock<IPhotoRepository>();
            _photoService = new PhotoService(_mockPhotoRepository.Object);
        }

        [TestMethod]
        public void ShowPhotos_ReturnPhotos_IfSubjectIdAndPhotoTypeExists()
        {
            //Arrange
            var listOfPhotos = GetPhotosTest();
            _mockPhotoRepository.Setup(p => p.GetPhotos(It.IsAny<int>(),It.IsAny<PhotoType>())).Returns(listOfPhotos);

            //Act
            var actual = _photoService.DisplayPhoto(subjectId, PhotoType.Hotel);

            //Assert
            Assert.AreEqual(2, actual.Count());
        }

        [TestMethod]
        public void ShowPhotos_ReturnZero_IfSubjectIdAndTypeDontExist()
        {
            //Arrange
            _mockPhotoRepository.Setup(p => p.GetPhotos(It.IsAny<int>(), It.IsAny<PhotoType>())).Returns(new List<Photo>());

            //Act
            var actual = _photoService.DisplayPhoto(subjectId,PhotoType.Hotel);

            //Assert
            Assert.IsFalse(actual.Any());
        }

        [TestMethod]
        public void AddPhotosToDatabase_Verify()
        {
            //Arrange
            var file = new Mock<HttpPostedFileBase>();
            file.Setup(x => x.FileName).Returns("fake.txt");
            file.Setup(x => x.ContentLength).Returns(45600);
            file.Setup(x => x.InputStream).Returns(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(file.Name)));

            //Act
            _photoService.AddPhoto(file.Object, PhotoType.Hotel, subjectId);

            //Assert
            _mockPhotoRepository.Verify(p => p.AddPhoto(It.IsAny<Photo>()));
        }

        public static List<Photo> GetPhotosTest()
        {
            int subjectId = RandomGenerator.Integer(1,2);

            return new List<Photo>()
            {
                new Photo()
                {
                    SubjectId = subjectId,
                    Type = PhotoType.Hotel
                },
                new Photo()
                {
                    SubjectId = subjectId,
                    Type = PhotoType.Hotel
                }
            };
        }
    }
}
