﻿using System;
using System.Collections.Generic;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVC.Services.ContractsServices;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class RoomBookingServiceUnitTest
    {
        private Mock<IRoomBookingRepository> _mockRoomBookingRepository;
        private RoomBookingService _roomBookingService;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockRoomBookingRepository = new Mock<IRoomBookingRepository>();
            _roomBookingService = new RoomBookingService(_mockRoomBookingRepository.Object);
        }

        [TestMethod]
        public void PopulateRoomBookingDatabase_Verify()
        {
            //Arrange
            var booking = GetModelTest();

            //Act
            _roomBookingService.BookARoom(booking);

            //Assert
            _mockRoomBookingRepository.Verify(r => r.BookRoom(booking));
        }

        public static RoomBookingModel GetModelTest()
        {
            var dateTo = RandomGenerator.Date(DateTime.Now);
            var dateFrom = RandomGenerator.Date(dateTo);
            int roomId = RandomGenerator.Integer(1,50);
            int userId = RandomGenerator.Integer(1,5);

            return new RoomBookingModel()
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                MealType = BookingMVC.Common.MealType.None,
                RoomId = roomId,
                UserId = userId
            };
        }
    }
}
