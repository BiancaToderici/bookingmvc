﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class RatingServiceUnitTest
    {
        private Mock<IRatingRepository> _mockRatingRepository;
        private RatingService _ratingService;
        private RatingModel ratingModel;
        private IEnumerable<RatingModel> ratingModels;
        const int userId = 1;
        const int unitId = 2;
        const int ratingId = 3;

        [TestInitialize]
        public void TestInitialize()
        {
            ratingModel = GetTestRatingModel();
            ratingModels = GetAllRatings();
            _mockRatingRepository = new Mock<IRatingRepository>();
            _ratingService = new RatingService(_mockRatingRepository.Object);
        }

        [TestMethod]
        public void PopulateDatabase_Verify()
        {
            //Arrange & Act
            _ratingService.Add(ratingModel);

            //Assert
            _mockRatingRepository.Verify(r => r.Add(It.IsAny<Rating>()));
        }

        [TestMethod]
        public void GetRatingsByUser_ReturnRatings_IfUserExists()
        {
            //Arrange
            _mockRatingRepository.Setup(r => r.GetUserRatingsExtended(userId)).Returns(ratingModels);

            //Act
            var actual = _ratingService.GetByUser(userId);

            //Assert
            Assert.AreEqual(2, actual.Count());
        }

        [TestMethod]
        public void GetRatingsByUser_ReturnsZero_IfUserDoesntExists()
        {
            //Arrange
            _mockRatingRepository.Setup(r => r.GetUserRatingsExtended(userId)).Returns(new List<RatingModel>());

            //Act
            var actual = _ratingService.GetByUser(userId);

            //Assert
            Assert.IsFalse(actual.Any());
        }

        [TestMethod]
        public void GetRatingAverage_ReturnsAverage_IfRatingExists()
        {
            //Arrange
             var expected = ratingModels.Average(l => l.Value);
            _mockRatingRepository.Setup(d => d.GetUnitRatings(unitId)).Returns(ratingModels);
            _mockRatingRepository.Setup(r => r.AverageRatings(ratingModels)).Returns(expected);

            //Act
            var actual = _ratingService.GetAverage(unitId);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetRatingsAverage_ReturnsNull_IfRatingDoesntExists()
        {
            //Arrange
            var expected = (double?)null;
            _mockRatingRepository.Setup(d => d.GetUnitRatings(unitId)).Returns(ratingModels);
            _mockRatingRepository.Setup(r => r.AverageRatings(ratingModels)).Returns(expected);

            //Act
            var actual = _ratingService.GetAverage(unitId);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void DeleteRating_Verify()
        {
            //Arrange & Act
            _ratingService.Delete(It.IsAny<int>());

            //Assert
            _mockRatingRepository.Verify(r => r.Delete(It.IsAny<int>()));
        }

        [TestMethod]
        public void GetRating_ReturnsRating_IfRatingIdExists()
        {
            //Arrange
            var expected = new Rating();
            _mockRatingRepository.Setup(r => r.GetById(ratingId)).Returns(expected);

            //Act
            var actual = _ratingService.GetById(ratingId);

            //Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void GetRating_ReturnsNull_IfRatingIdDoesntExists()
        {
            //Arrange & Act
            var actual = _ratingService.GetById(ratingId);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetRatingsByUnit_ReturnRatings_IfRatingExists()
        {
            //Arrange
            _mockRatingRepository.Setup(r => r.GetUnitRatings(unitId)).Returns(ratingModels);

            //Act
            var actual = _ratingService.GetByUnit(unitId);

            //Assert
            Assert.AreEqual(2, actual.Count());
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Any());
        }

        [TestMethod]
        public void GetRatingsByUnit_ReturnNull_IfRatingDoesntExists()
        {
            //Arrange
            _mockRatingRepository.Setup(d => d.GetUnitRatings(unitId)).Returns(new List<RatingModel>());

            //Act
            var actual = _ratingService.GetByUnit(unitId);

            //Assert
            Assert.IsFalse(actual.Any());
        }


        private static RatingModel GetTestRatingModel()
        {
            return new RatingModel
            {
                BookingId = RandomGenerator.Integer(1,10),
                Id = RandomGenerator.Integer(10,20),
                Date = DateTime.Now,
                Details  = RandomGenerator.String(),
                UserId = RandomGenerator.Integer(1,9)
            };
        }
        
        private static IEnumerable<RatingModel> GetAllRatings()
        {
            var rating = new RatingModel()
            {
                Id = RandomGenerator.Integer(1,5),
                Value = RandomGenerator.Integer(1,5)
            };

            var rating1 = new RatingModel()
            {
                Id = RandomGenerator.Integer(1, 5),
                Value = RandomGenerator.Integer(1, 5)
            };

            var ratings = new List<RatingModel>
            {
                rating, rating1
            };

            return ratings;
        }
    }
}
