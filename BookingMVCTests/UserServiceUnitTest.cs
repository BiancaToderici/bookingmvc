﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class UserServiceUnitTest
    {
        private Mock<IUserRepository> _mockUserRepository;
        private UserService _userService;
        const string userName = "user";
        User firstUser = GetUsersTest().First();
        User lastUser = GetUsersTest().Last();

        [TestInitialize]
        public void TestInitialize()
        {
            _mockUserRepository = new Mock<IUserRepository>();
            _userService = new UserService(_mockUserRepository.Object);
        }

        [TestMethod]
        public void AuthenticateUser_ReturnTrue_IfCredentialsAreValid()
        {
            //Arrange
            var expected = firstUser;
            _mockUserRepository.Setup(u => u.GetUser(It.IsAny<string>())).Returns(expected);

            //Act
            var actual = _userService.IsAuthorized(ref expected);

            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AuthenticateUser_ReturnFalse_IfCredentialsAreInvalid()
        {
            //Arrange
            var expected = firstUser;
            _mockUserRepository.Setup(u => u.GetUser(lastUser.UserName)).Returns(expected);

            //Act
            var actual = _userService.IsAuthorized(ref lastUser);

            //Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void UserIsAdmin_ReturnTrue_IfCredentialsAreValid()
        {
            //Arrange
            var expected = firstUser;
            _mockUserRepository.Setup(u => u.GetUser(It.IsAny<string>())).Returns(expected);

            //Act
            var actual = _userService.IsAdmin(firstUser.UserName);

            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void UserIsAdmin_ReturnFalse_IfCredentialsAreInvalid()
        {
            //Arrange
            var expected = lastUser;
            _mockUserRepository.Setup(u => u.GetUser(It.IsAny<string>())).Returns(expected);

            //Act
            var actual = _userService.IsAdmin(firstUser.UserName);

            //Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void UserIsRegular_ReturnTrue_IfCredentialsAreValid()
        {
            //Arrange
            var expected = lastUser;
            _mockUserRepository.Setup(u => u.GetUser(It.IsAny<string>())).Returns(expected);

            //Act
            var actual = _userService.IsRegular(expected.UserName);

            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void UserIsRegular_ReturnFalse_IfCredentialsAreInvalid()
        {
            //Arrange
            var expected = firstUser;
            _mockUserRepository.Setup(u => u.GetUser(It.IsAny<string>())).Returns(expected);

            //Act
            var actual = _userService.IsRegular(lastUser.UserName);

            //Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void Get_ReturnUser_IfUserNameExists()
        {
            //Arrange
            var expected = firstUser;
            _mockUserRepository.Setup(u => u.GetUser(userName)).Returns(expected);

            //act
            var actual = _userService.GetUser(userName);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Get_ReturnNull_IfUserNAmeDoesntExists()
        {
            //Arrange
            _mockUserRepository.Setup(u => u.GetUser(userName));

            //Act
            var actual = _userService.GetUser(userName);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void DeleteUser_Verify()
        {
            //Arrange
            int id = RandomGenerator.Integer(1,10);

            //Act
            _userService.Delete(id);

            //Assert
            _mockUserRepository.Verify(u => u.Delete(id));
        }

        [TestMethod]
        public void AddUser_Verify()
        {
            //Arrange & Act
            _userService.Add(new UserModel());

            //Assert
            _mockUserRepository.Verify(u => u.Add(It.IsAny<User>()));
        }

        [TestMethod]
        public void UserExists_ReturnTrue_IfUserNameExists()
        {
            //Arrange
            string userName = RandomGenerator.String();
            var expected = firstUser;
            _mockUserRepository.Setup(r => r.GetUser(userName)).Returns(expected);

            //Act
            var actual = _userService.UserExists(userName);

            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void UserExists_ReturnFalse_IfUserNameDoesNotExists()
        {
            //Arrange
            string userName = RandomGenerator.String();
            _mockUserRepository.Setup(r => r.GetUser(userName));

            //Act
            var actual = _userService.UserExists(userName);

            //Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void GetAllUsersFromDB()
        {
            //Arrange
            var expected = GetUsersTest();
            _mockUserRepository.Setup(u => u.GetList()).Returns(expected);

            //Act
            var actual = _userService.GetList();

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Edit_Verify()
        {
            //Arrange & Act
            _userService.Edit(new UserModel());

            //Assert
            _mockUserRepository.Verify(u => u.Edit(It.IsAny<User>()));
        }

        [TestMethod]
        public void GetById_ReturnUser_IfIdExists()
        {
            //Arrange
            int id = RandomGenerator.Integer(1,5);
            var expected = firstUser;
            _mockUserRepository.Setup(u => u.GetById(id)).Returns(expected);

            //Act
            var actual = _userService.GetById(id);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetById_ReturnNull_IfIdDoesNotExists()
        {
            //Arrange
            int id = RandomGenerator.Integer(1, 5);
            _mockUserRepository.Setup(u => u.GetById(id));

            //Act
            var actual = _userService.GetById(id);

            //Assert
            Assert.IsNull(actual);
        }


        public static List<User> GetUsersTest()
        {

            return new List<User>()
            {
                new User()
                {
                    Id = RandomGenerator.Integer(1,10),
                    UserName = RandomGenerator.String(),
                    Password = RandomGenerator.String(),
                    Type = UserType.Administrator
                },
                new User()
                {
                    Id = RandomGenerator.Integer(1,9),
                    UserName = RandomGenerator.String(),
                    Password = RandomGenerator.String(),
                    Type = UserType.RegularUser
                }
            };
        }
    }
}
