﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class UnitServiceUnitTest
    {
        private Mock<IUnitRepository> _unitRepositoryMock;
        private Mock<IRoomRepository> _roomRepositoryMock;
        private UnitService _unitService;

        [TestInitialize]
        public void TestInitialize()
        {
            _unitRepositoryMock = new Mock<IUnitRepository>();
            _roomRepositoryMock = new Mock<IRoomRepository>();
            _unitService = new UnitService(_unitRepositoryMock.Object,_roomRepositoryMock.Object);
        }

        [TestMethod]
        public void ShowAllUnitsFromDatabase()
        {
            //Arrange
            var expected = GetAllUnitsTest();
            _unitRepositoryMock.Setup(u => u.GetList()).Returns(expected);

            //Act
            var actual = _unitService.GetUnits();

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetFreeUnits_ReturnUnits_IfUnitsRoomAreFree()
        {
            //Arrange
            string search = RandomGenerator.String();
            DateTime? dateFrom = null;
            DateTime? dateTo = null;
            var units = GetAllUnitsTest();
            var rooms = GetRoomsTest();
           
            _unitRepositoryMock.Setup(u => u.GetUnits(search)).Returns(units);

            for (int i = 0; i < units.Count; i++)
            {
                int unitId = units[i].Id;
                var unitRooms = rooms.Where(r => r.UnitId == unitId);
                _roomRepositoryMock.Setup(r => r.GetRooms(unitId)).Returns(unitRooms);
            }

            var roomBooked = rooms.First();
            _roomRepositoryMock.Setup(r => r.IsBooked(roomBooked, dateFrom, dateTo)).Returns(true);

            //Act
            var freeUnits = _unitService.GetFreeUnits(search, dateFrom, dateTo);

            //Assert
            Assert.AreEqual(freeUnits.Count(), 2);
            Assert.IsFalse(freeUnits.Select(u => u.Id).Contains(roomBooked.UnitId));
        }

        [TestMethod]
        public void GetFreeUnits_ReturnNull_IfUnitsRoomAreBooked()
        {
            //Arrage
            string search = RandomGenerator.String();
            DateTime? dateFrom = null;
            DateTime? dateTo = null;
            var units = GetAllUnitsTest();
            var rooms = GetRoomsTest();

            _unitRepositoryMock.Setup(u => u.GetUnits(search)).Returns(units);

            for (int i = 0; i < units.Count; i++)
            {
                int unitId = units[i].Id;
                var unitRooms = rooms.Where(r => r.UnitId == unitId);
                _roomRepositoryMock.Setup(r => r.GetRooms(unitId)).Returns(unitRooms);
            }

            var roomBooked = rooms;
            for (int i = 0; i < roomBooked.Count; i++)
            {
                _roomRepositoryMock.Setup(r => r.IsBooked(roomBooked[i], dateFrom, dateTo)).Returns(true);
            }
            
            //Act
            var freeUnits = _unitService.GetFreeUnits(search, dateFrom, dateTo);

            //Assert
            Assert.IsFalse(freeUnits.Any());
        }

        [TestMethod]
        public void SearchUnit_UnitExists_IsNotNull()
        {
            //Arrange
            string search = RandomGenerator.String();
            var expected = GetAllUnitsTest();
            _unitRepositoryMock.Setup(u => u.GetUnits(search)).Returns(expected);

            //Act
            var actual = _unitService.SearchList(search);

            //Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void SearchUnit_UnitDoesntExists_IsNull()
        {
            //Arrange
             string search = RandomGenerator.String();
            _unitRepositoryMock.Setup(u => u.GetUnits(search));

            //Act
            var actual = _unitService.SearchList(search);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void UserOffers_Exist_IsNotNull()
        {
            //Arrange
            int id = RandomGenerator.Integer(1,5);
            var expected = GetAllUnitsTest();
            _unitRepositoryMock.Setup(u => u.UserOffers(id)).Returns(expected);

            //Act
            var actual = _unitService.UserOffers(id);

            //Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void UserOffers_DoesntExist_ReturnNull()
        {
            //Arrange
            int id = RandomGenerator.Integer(1,5);
            _unitRepositoryMock.Setup(u => u.UserOffers(id));

            //Act
            var actual = _unitService.UserOffers(id);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void UnitNameById_NotNull()
        {
            //Arrange
            int id = RandomGenerator.Integer(1,5);
            var expected = "not null";
            _unitRepositoryMock.Setup(u => u.GetNameById(id)).Returns(expected);

            //Act
            var actual = _unitService.GetNameById(id);

            //Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void UnitExists_ReturnTrue_IfNameExists()
        {
            //Arrange
            string name = RandomGenerator.String();
            var expected = GetUnitTest();
            _unitRepositoryMock.Setup(s => s.GetName(name)).Returns(expected);

            //Act
            var actual = _unitService.UnitExist(name);

            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void UnitExists_ReturnFalse_IfNameDoesntExists()
        {
            //Arrange
            string name = RandomGenerator.String();
            _unitRepositoryMock.Setup(u => u.GetName(name));

            //act
            var actual = _unitService.UnitExist(name);

            //Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void DeleteUnit()
        {
            //Arrange
            int id = RandomGenerator.Integer(1,5);

            //Act
            _unitService.Delete(id);

            //Assert
            _unitRepositoryMock.Verify(u => u.DeleteUnit(id));
        }
        
        private static List<Unit> GetAllUnitsTest()
        {
            return new List<Unit>()
            {
                new Unit()
                {
                     Id = 1,
                     Address = RandomGenerator.String(),
                     Name = RandomGenerator.String(),
                     Stars = RandomGenerator.Integer(1,6)
                },
                new Unit ()
                {
                    Id = 2,
                     Address = RandomGenerator.String(),
                     Name = RandomGenerator.String(),
                     Stars = RandomGenerator.Integer(1,6)
                },
                new Unit ()
                {
                     Id = 3,
                     Address = RandomGenerator.String(),
                     Name = RandomGenerator.String(),
                     Stars = RandomGenerator.Integer(1,6)
                }
            };
        }

        private static Unit GetUnitTest()
        {
            return new Unit()
            {
                Id = RandomGenerator.Integer(1, 8),
                Address = RandomGenerator.String(),
                Name = RandomGenerator.String(),
                Stars = RandomGenerator.Integer(1, 6)
            };
        }

        private static List<Room> GetRoomsTest()
        {
            return new List<Room>()
            {
                new Room()
                {
                    Details =RandomGenerator.String(),
                    Id = RandomGenerator.Integer(1,5),
                    NrPersons = RandomGenerator.Integer(1,4),
                    Price = RandomGenerator.Integer(100,150),
                    RoomNr = RandomGenerator.Integer(1,20),
                    UnitId = 1
                },
                 new Room()
                {
                    Details =RandomGenerator.String(),
                    Id = RandomGenerator.Integer(1,5),
                    NrPersons = RandomGenerator.Integer(1,4),
                    Price = RandomGenerator.Integer(100,150),
                    RoomNr = RandomGenerator.Integer(1,20),
                    UnitId = 2
                },
                  new Room()
                {
                    Details =RandomGenerator.String(),
                    Id = RandomGenerator.Integer(1,5),
                    NrPersons = RandomGenerator.Integer(1,4),
                    Price = RandomGenerator.Integer(100,150),
                    RoomNr = RandomGenerator.Integer(1,20),
                    UnitId = 3
                }
            };
        }
    }
}
