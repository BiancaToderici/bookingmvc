﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using BookingMVC.Common;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests
{
    [TestClass]
    public class FileLoggerUnitTest
    {
        private FileLogger _fileLogger;
        private static Random random = new Random();
        private DateTime _dateTime;
        private string _path;
        const string stacktrace = "trace";


        [TestInitialize]
        public void TestInitialize()
        {
            _path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "Logs\\logger.txt");
            _fileLogger = new FileLogger();
            _dateTime = DateTime.Now;
        }

        [TestMethod]
        public void SaveDebugMessageToFile_ReturnTrue()
        {
            //Arrange
            var message = RandomGenerator.String();
            var dateTime = _dateTime.ToString();

            //Act
            _fileLogger.Debug(message);

            //Assert
            var result = SearchString(message, dateTime);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SaveErrorMessageToFile_ReturnTrue()
        {
            //Arrange
            var message = RandomGenerator.String();
            var dateTime = _dateTime.ToString();

            //Act
            _fileLogger.Error(message, stacktrace);

            //Assert
            var result = SearchString(message, dateTime);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SaveInformationMessageToFile_ReturnTrue()
        {
            //Arrange
            var message = RandomGenerator.String();
            var dateTime = _dateTime.ToString();

            //Act
            _fileLogger.Information(message);

            //Assert
            var result = SearchString(message, dateTime);
            Assert.IsTrue(result);
        }

        private bool SearchString(string message, string dateTime)
        {
            bool exist;
            var result = File.ReadAllLines(_path).Select(l => l.Contains(message) && l.Contains(dateTime));
            if (result.Contains(true))
            {
                exist = true;
            }
            else
            {
                exist = false;
            }
            return exist;
        }
    }
}
