﻿using System;
using System.Configuration;
using BookingMVC.Common;
using BookingMVC.Repositories.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class DbLoggerUnitTest
    {
        private Mock<ILogRepository> _mockLogRepository;
        private DbLogger _dbLogger;
        const string message = "logger";

        [TestInitialize]
        public void TestInitialize()
        {
            _mockLogRepository = new Mock<ILogRepository>();
            _dbLogger = new DbLogger(_mockLogRepository.Object);
        }

        [TestMethod]
        public void SaveDebugMessageToDB()
        {
            //Act
            _dbLogger.Debug(message);

            //Assert
            _mockLogRepository.Verify(l => l.Save(It.IsAny<Log>()));
        }

        [TestMethod]
        public void SaveErrorMessageToDB()
        {
            //Act
            _dbLogger.Debug(message);

            //Assert
            _mockLogRepository.Verify(l => l.Save(It.IsAny<Log>()));
        }

        [TestMethod]
        public void SaveInformationMessageToDB()
        {
            //Act
            _dbLogger.Debug(message);

            //Assert
            _mockLogRepository.Verify(l => l.Save(It.IsAny<Log>()));
        }
    }
}