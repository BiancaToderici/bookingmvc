﻿using System;
using System.Collections.Generic;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class RoomServiceUnitTest
    {
        private Mock<IRoomRepository> _roomRepositoryMock;

        private RoomService _roomService;

        [TestInitialize]
        public void TestInitialize()
        {
            _roomRepositoryMock = new Mock<IRoomRepository>();
            _roomService = new RoomService(_roomRepositoryMock.Object);
        }

        [TestMethod]
        public void ShowRoomsForSpecificUnit()
        {
            // Arrange
            const int unitId = 3;
            var expected = GetTestRooms();
            _roomRepositoryMock.Setup(r => r.GetRooms(unitId)).Returns(expected);

            // Act
            var actual = _roomService.GetRooms(unitId);

            // Assert
            _roomRepositoryMock.Verify(v => v.GetRooms(unitId));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetById_ReturnSpecificRoom_IfRoomIdExists()
        {
            //Arrange
            int roomId = RandomGenerator.Integer(1,100);
            var expected = GetRoomTest();
            _roomRepositoryMock.Setup(r => r.GetById(roomId)).Returns(expected);

            //Act
            var actual = _roomService.GetById(roomId);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetById_ReturnNull_IfRoomIdDoesntExists()
        {
            //Arrange
            int roomId = RandomGenerator.Integer(1, 100);

            //Act
            var actual = _roomService.GetById(roomId);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void RoomNrExist_ReturnTrue_IfRoomExists()
        {
            //Arrange
            int roomNr = RandomGenerator.Integer(1, 100);
            int id = RandomGenerator.Integer(1, 100);
            var expected = GetRoomTest();
            _roomRepositoryMock.Setup(r => r.GetNr(roomNr, id)).Returns(expected);

            //Act
            var actual = _roomService.RoomNrExists(roomNr, id);

            //Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void RoomNrExists_ReturnFalse_IfRoomDoesNotExists()
        {
            //Arrange
            int roomNr = RandomGenerator.Integer(1, 100);
            int id = RandomGenerator.Integer(1, 100);

            //Act
            var actual = _roomService.RoomNrExists(roomNr, id);

            //Assert
            Assert.IsFalse(actual);
        }
   
        [TestMethod]
        public void GetRoomNr_ReturnSpecificNumber()
        {
            //Arrange
            int roomId = RandomGenerator.Integer(1, 100);
            int expected = RandomGenerator.Integer(1, 100);
            _roomRepositoryMock.Setup(r => r.RoomNr(roomId)).Returns(expected);

            //Act
            var actual = _roomService.GetRoomNr(roomId);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DeleteRoom()
        {
            //Arrange
            int id = RandomGenerator.Integer(1, 100);

            //Act
            _roomService.Delete(id);

            //Assert
            _roomRepositoryMock.Verify(r => r.Delete(id));
        }

        private static List<Room> GetTestRooms()
        {
            return new List<Room>()
            {
                new Room()
                {
                    Id= RandomGenerator.Integer(1, 10),
                    Details = RandomGenerator.String(),
                    NrPersons = RandomGenerator.Integer(1,3),
                    Price = RandomGenerator.Integer(120,160),
                    RoomNr = RandomGenerator.Integer(1, 10),
                    Type = RandomGenerator.String(),
                    UnitId = RandomGenerator.Integer(1,200)
                },
                new Room()
                {
                    Id= RandomGenerator.Integer(1, 10),
                    Details = RandomGenerator.String(),
                    NrPersons = RandomGenerator.Integer(1,3),
                    Price = RandomGenerator.Integer(120,160),
                    RoomNr = RandomGenerator.Integer(1, 10),
                    Type = RandomGenerator.String(),
                    UnitId = RandomGenerator.Integer(1,200)
                }
            };
        }

        private static Room GetRoomTest()
        {
            return new Room()
            {
                Id = RandomGenerator.Integer(1, 10),
                Details = RandomGenerator.String(),
                NrPersons = RandomGenerator.Integer(1, 3),
                Price = RandomGenerator.Integer(120, 160),
                RoomNr = RandomGenerator.Integer(1, 10),
                Type = RandomGenerator.String(),
                UnitId = RandomGenerator.Integer(1, 200)
            };
        }
    }
}
