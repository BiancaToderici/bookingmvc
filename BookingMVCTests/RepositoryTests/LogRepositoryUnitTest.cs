﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Common;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests.RepositoryTests
{
    [TestClass]
    public class LogRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private LogRepository _logRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            _logRepository = new LogRepository();
        }

        [TestMethod]
        public void AddLog()
        {
            TransactionScope(() =>
            {
                //Arrange
                var expected = LogTest();

                //Act
                _logRepository.Save(expected);

                using( var db = new LoggerDbContext(LoggerConnectionString))
                {
                    var actual = db.Log.Where(l => l.Id == expected.Id).SingleOrDefault();

                    //Assert
                    Assert.AreEqual(expected.Message, actual.Message);
                }
            });
        }

        private static Log LogTest()
        {
            return new Log
            {
                Message = RandomGenerator.String(),
                Timestamp = RandomGenerator.Date(DateTime.Now)
            };
        }
    }
}
