﻿using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Transactions;

namespace BookingMVCTests.RepositoryTests
{
    public class BaseRepositoryUnitTest
    {
        private UnitRepository _unitRepository;
        private RoomRepository _roomRepository;
        private RoomBookingRepository _roomBookingRepository;
        private RatingRepository _ratingRepository;
        private TransactionScope _scope;
        private string connectionString;

        public BaseRepositoryUnitTest()
        {
            _scope = new TransactionScope();
            _unitRepository = new UnitRepository();
            _roomRepository = new RoomRepository();
            _roomBookingRepository = new RoomBookingRepository();
            _ratingRepository = new RatingRepository();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _scope.Dispose();
        }

        public string BookingConnectionString
        {
            get { return connectionString = ConnectionString.GetBookingConnectionString(); }
        }

        public string LoggerConnectionString
        {
            get { return connectionString = ConnectionString.GetLoggerConnectionString(); }
        }

        public Unit GetTestUnit()
        {
            var unit = GenerateUnit();
            _unitRepository.Add(unit);
            return unit;
        }

        public Room GetTestRoom(int unitId)
        {
            Room room = GenerateRoom(unitId);
            var result = _roomRepository.AddRoom(new RoomModel
            {
                Details = room.Details,
                NrPersons = room.NrPersons,
                Price = room.Price,
                RoomNr = room.RoomNr,
                Type = room.Type,
                UnitId = room.UnitId
            });
            return result;
        }

        public RoomBooking GetTestRoomBooking(int roomId)
        {
            RoomBooking roomBooking = GenerateBooking(roomId);
            var result = _roomBookingRepository.BookRoom(new RoomBookingModel
            {
                DateFrom = roomBooking.DateFrom,
                DateTo = roomBooking.DateTo,
                MealType = roomBooking.MealType,
                RoomId = roomBooking.RoomId,
                UserId = roomBooking.UserId
            });
            return result;
        }

        public Rating GetTestRating(int bookingId, int userId)
        {
            Rating rating = GenerateReview(bookingId, userId);
            _ratingRepository.Add(rating);
            return rating;
        }

        private static Unit GenerateUnit()
        {
            return new Unit
            {
                Address = RandomGenerator.String(20),
                Name = RandomGenerator.String(10),
                Type = UnitType.Hotel,
                Contact = RandomGenerator.Integer(10,5).ToString()
            };
        }

        private static Room GenerateRoom(int unitId)
        {
            return new Room
            {
                RoomNr = RandomGenerator.Integer(0,15),
                Type = RandomGenerator.String(),
                Price = RandomGenerator.Integer(0,150),
                UnitId = unitId
            };
        }

        private static RoomBooking GenerateBooking(int roomId)
        {
            var dateTo = RandomGenerator.Date(DateTime.Now);
            var dateFrom = RandomGenerator.Date(dateTo);
            return new RoomBooking
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                RoomId = roomId,
                MealType = MealType.All,
                UserId = RandomGenerator.Integer(1, 3)
            };
        }

        private static Rating GenerateReview(int bookingId,int userId)
        {
            return new Rating
            { 
                Date = RandomGenerator.Date(DateTime.Now),
                Value = RandomGenerator.Integer(1,5),
                Details = RandomGenerator.String(),
                BookingId = bookingId,
                UserId = userId
            };
        }
        public void TransactionScope(Action action)
        {
            using (var scope = new TransactionScope())
            {
                action();

                scope.Complete();
            }
        }

    }
}
