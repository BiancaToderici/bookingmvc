﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Common;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests.RepositoryTests
{
    [TestClass]
    public class HistoryRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private HistoryRepository _historyRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            _historyRepository = new HistoryRepository();
        }

        [TestMethod]
        public void Add()
        {
            TransactionScope(() =>
            {
                //Arrange
                var expected = GetHistoryTest();

                //Act
                _historyRepository.SaveHistory(expected);

                using (var db  = new HistoryDbContext(BookingConnectionString))
                {
                    var actual = db.History.Where(h => h.Id == expected.Id).SingleOrDefault();

                    //Assert
                    Assert.AreEqual(expected.RoomId, actual.RoomId);
                }
            });

        }

        [TestMethod]
        public void GetUserRatings_ReturnRatings_IfRatingsExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var expected = GetTestRoomBooking(room.Id);

                //Act
                var actualBookings = _historyRepository.History(expected.UserId);

                //Assert
                var actual = actualBookings.SingleOrDefault(r => r.RoomBooking.Id == expected.Id);
                Assert.AreEqual(expected.RoomId, actual.RoomBooking.RoomId);
            });
        }

        private static History GetHistoryTest()
        {
            return new History
            {
                RoomId = RandomGenerator.Integer(150,300)
            };
        }
    }
}
