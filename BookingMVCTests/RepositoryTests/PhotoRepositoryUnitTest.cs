﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Common;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests.RepositoryTests
{
    [TestClass]
    public class PhotoRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private PhotoRepository _photoRepository;
        private Photo photoTest;

        [TestInitialize]
        public void TestInitialize()
        {
            photoTest = GetPhotoTest();
            _photoRepository = new PhotoRepository();
        }

        [TestMethod]
        public void Add_SavePhotoToDB()
        {
            TransactionScope(() =>
            {
                //Act
                _photoRepository.AddPhoto(photoTest);

                using(var db = new DbEntitiesDbContext(BookingConnectionString))
                {
                    var photo = db.Photos.Where(r => r.Id == photoTest.Id).ToList();

                    //Assert
                    Assert.IsTrue(photo.Any());
                }
            });
        }

        [TestMethod]
        public void ShowPhotosFromDatabase_ReturnTrue_IfSubjectIdAndPhotoTypeExist()
        {
            TransactionScope(() =>
            {
                _photoRepository.AddPhoto(photoTest);

                //Act
                var result = _photoRepository.GetPhotos(photoTest.SubjectId, PhotoType.Hotel);

                //Assert
                Assert.IsTrue(result.Any());
            });
        }

        [TestMethod]
        public void ShowPhotosFromDatabase_ReturnFalse_IfSubjectIdAndPhotoTypeDoesNotExist()
        {
            //Arrange
            const int subjectId = -100;

            TransactionScope(() =>
            {
                _photoRepository.AddPhoto(photoTest);

                //Act
                var result = _photoRepository.GetPhotos(subjectId, PhotoType.Hotel);

                //Assert
                Assert.IsFalse(result.Any());
            });
        }

        private static Photo GetPhotoTest()
        {
            return new Photo
            {
                Photos = RandomGenerator.ByteArray(),
                SubjectId = RandomGenerator.Integer(1,2),
                Type = PhotoType.Hotel
            };
        }
    }
}
