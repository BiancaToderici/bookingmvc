﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVCTests
{
    public static class ConnectionString
    {
        public static string GetBookingConnectionString()
        {
            string connectionString;
            try
            {
                return connectionString = ConfigurationManager.ConnectionStrings["BookingConnectionString"].ConnectionString;
            }
            catch (Exception e)
            {
                throw new ApplicationException($"Unable to get DB Connection string from App.Config {e.Message}");
            }
        }

        public static string GetLoggerConnectionString()
        {
            string connectionString;
            try
            {
                return connectionString = ConfigurationManager.ConnectionStrings["LoggerConnectionString"].ConnectionString;
            }
            catch (Exception e)
            {
                throw new ApplicationException($"Unable to get DB Connection string from App.Config {e.Message}");
            }
        }
    }
}
