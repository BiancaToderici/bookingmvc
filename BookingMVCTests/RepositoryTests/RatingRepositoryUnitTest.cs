﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests.RepositoryTests
{
    [TestClass]
    public class RatingRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private RatingRepository _ratingRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            _ratingRepository = new RatingRepository();
        }

        [TestMethod]
        public void Add()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);

                //Act
                var expected = GetTestRating(booking.Id, booking.UserId);

                using (var db = new RatingDbContext(BookingConnectionString))
                {
                    var actual = db.Rating.Where(r => r.Details == expected.Details).SingleOrDefault();

                    //Assert
                    Assert.AreEqual(expected.Id, actual.Id);
                }
            });
        }

        [TestMethod]
        public void Edit()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var rating = GetTestRating(booking.Id, booking.UserId);
                Rating expected = new Rating
                {
                    Id = rating.Id,
                    Value = RandomGenerator.Integer(1,5),
                    Details = RandomGenerator.String(),
                    Date = RandomGenerator.Date(DateTime.Now)
                };

                //Act
                _ratingRepository.Edit(expected);

                using(var db = new RatingDbContext(BookingConnectionString))
                {
                    var actual = db.Rating.Where(r => r.Id == expected.Id).SingleOrDefault();

                    //Assert
                    Assert.AreEqual(expected.Details, actual.Details);
                }
            });
        }

        [TestMethod]
        public void Delete()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var rating = GetTestRating(booking.Id,booking.UserId);

                //Act
                _ratingRepository.Delete(rating.Id);

                using (var db = new RatingDbContext(BookingConnectionString))
                {
                    var actual = db.Rating.Where(r => r.Id == rating.Id).SingleOrDefault();

                    //Assert
                    Assert.IsNull(actual);
                }

            });
        }

        [TestMethod]
        public void GetRatings_ReturnRating_IfRatingExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var rating = GetTestRating(booking.Id, booking.UserId);

                //Act
                var actual = _ratingRepository.GetRatings(rating.BookingId);

                //Assert
                Assert.IsTrue(actual.Any());
            });
        }

        [TestMethod]
        public void GetRatings_ReturnNull_IfRatingDoesNotExists()
        {
            //Arrange
            const int bookingId = -100;

            //Act
            var actual = _ratingRepository.GetRatings(bookingId);
           
            //Assert
             Assert.IsFalse(actual.Any());
        }

        [TestMethod]
        public void GetUserRatings_ReturnRating_IfReviewExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var expected = GetTestRating(booking.Id, booking.UserId);

                //Act
                var actualRatings = _ratingRepository.GetUserRatings(expected.UserId);

                //Assert
                var actual = actualRatings.Where(r => r.Id == expected.Id).SingleOrDefault();
                Assert.AreEqual(expected.Value, actual.Value);
            });
        }

        [TestMethod]
        public void GetUserRatings_ReturnNull_IfRatingDoesNotExists()
        {
            //Arrange
            const int userId = -4;

            //Act
            var result = _ratingRepository.GetUserRatings(userId);

            //Assert
             Assert.IsFalse(result.Any());
        }

        [TestMethod]
        public void GetById_ReturnRating_IfRatingExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var expected = GetTestRating(booking.Id, booking.UserId);

                //Act
                var actual = _ratingRepository.GetById(expected.Id);

                //Assert
                Assert.AreEqual(expected.Value, actual.Value);
            });
        }

        [TestMethod]
        public void GetAverage_ReturnResult()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var rating = GetTestRating(booking.Id, booking.UserId);
                var rating1 = GetTestRating(booking.Id, booking.UserId);
                List<RatingModel> ratings = new List<RatingModel>
                {
                    new RatingModel
                    {
                        BookingId = rating.BookingId,
                        Date = rating.Date,
                        Details = rating.Details,
                        UserId = rating.UserId,
                        Value = rating.Value
                    },
                    new RatingModel
                    {
                        BookingId = rating1.BookingId,
                        Date = rating1.Date,
                        Details = rating1.Details,
                        UserId = rating1.UserId,
                        Value = rating1.Value
                    }
                };
                double? expected = ratings.Average(r => r.Value);

                //Act
                var actual = _ratingRepository.AverageRatings(ratings);

                //Assert
                Assert.AreEqual(expected, actual);
            });
        }

        [TestMethod]
        public void GetUnitRatings_ReturnRatings_IfRatingsExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var expected = GetTestRating(booking.Id, booking.UserId);

                //Act
                var actualRatings = _ratingRepository.GetUnitRatings(room.UnitId);

                //Assert
                var actual = actualRatings.SingleOrDefault(r => r.Id == expected.Id);
                Assert.AreEqual(expected.Value, actual.Value);
            });
        }

        [TestMethod]
        public void GetUserRatings_ReturnRatings_IfRatingsExists()
        {

            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var expected = GetTestRating(booking.Id, booking.UserId);

                //Act
                var actualRatings = _ratingRepository.GetUserRatingsExtended(expected.UserId);

                //Assert
                var actual = actualRatings.SingleOrDefault(a => a.Id == expected.Id);
                Assert.AreEqual(expected.Value, actual.Value);
            });
        }

        [TestMethod]
        public void GetBookingRatings_ReturnRatings_IfRatingsExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);
                var expected = GetTestRating(booking.Id, booking.UserId);

                //Act
                var actualRatings = _ratingRepository.GetBookingRatingsExtended(expected.BookingId);

                //Assert
                var actual = actualRatings.SingleOrDefault(a => a.Id == expected.Id);
                Assert.AreEqual(actual.Value, expected.Value);
            });
        }
    }
}
