﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Common;
using BookingMVC.Repositories;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests
{
    [TestClass]
    public class UserRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private UserRepository _userRepository;
        private User user = GetUserTest();

        [TestInitialize]
        public void TestInitialize()
        {
            _userRepository = new UserRepository();
        }

        [TestMethod]
        public void GetUser_ReturnUser_IfNameExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);

                using(var db = new UserDbContext(BookingConnectionString))
                {
                    var expected = db.Users.Where(u => u.Id == user.Id).SingleOrDefault();
                    //Act
                    var actual = _userRepository.GetUser(expected.UserName);

                    //Assert
                    Assert.AreEqual(expected.Id, actual.Id);
                }
            });
        }

        [TestMethod]
        public void GetUser_ReturnNull_IfNameDoesNotExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);
                _userRepository.Delete(user.Id);

                //Act
                var actual = _userRepository.GetUser(user.UserName);

                //Assert
                Assert.IsNull(actual);
            });
        }

        [TestMethod]
        public void GetUserName_ReturnName_IfIdExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);

                using(var db = new UserDbContext(BookingConnectionString))
                {
                    var expected = db.Users.SingleOrDefault(u => u.Id == user.Id);

                    //Act
                    var actual = _userRepository.GetNameById(expected.Id);

                    //Assert
                    Assert.AreEqual(expected.UserName, actual);
                }
            });
        }

        [TestMethod]
        public void GetUsername_ReturnNull_IfIdDoesNotExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                const int id = 100;
               _userRepository.Delete(user.Id);

                //Act
                var actual = _userRepository.GetNameById(id);

                //Assert
                Assert.IsNull(actual);
            });
        }

        [TestMethod]
        public void Add_Verify()
        {
            TransactionScope(() =>
            {
                //Act
                _userRepository.Add(user);

                using(var db = new UserDbContext(BookingConnectionString))
                {
                    var actual = db.Users.SingleOrDefault(u => u.Id == user.Id);

                    //Assert
                    Assert.AreEqual(user.Id, actual.Id);
                }
            });
        }

        [TestMethod]
        public void Edit()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);
                User expected = new User
                {
                    Id = user.Id,
                    Password = RandomGenerator.String(),
                    Type = UserType.Guest,
                    UserName = RandomGenerator.String()
                };

                //Act
                _userRepository.Edit(expected);

                using( var db = new UserDbContext(BookingConnectionString))
                {
                    var actual = db.Users.SingleOrDefault(u => u.Id == expected.Id);

                    //Assert
                    Assert.AreEqual(expected.UserName, actual.UserName);
                }
            });
        }

        [TestMethod]
        public void Delete()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);

                //Act
                _userRepository.Delete(user.Id);

                using(var db = new UserDbContext(BookingConnectionString))
                {
                    var actual = db.Users.SingleOrDefault(u => u.Id == user.Id);

                    //Assert
                    Assert.IsNull(actual);
                }
            });
        }

        [TestMethod]
        public void GetById_ReturnUser_IfIdExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);

                //Act
                var actual = _userRepository.GetById(user.Id);

                //Assert
                var expected = user.UserName;
                Assert.AreEqual(expected, actual.UserName);
            });
        }

        [TestMethod]
        public void GetById_ReturnNull_IfIdDoesNotExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);
                _userRepository.Delete(user.Id);

                //Act
                var actual = _userRepository.GetById(user.Id);

                //Assert
                Assert.IsNull(actual);
            });
        }

        [TestMethod]
        public void GetUsersFromDb_ReturnTrue_IfUsersExist()
        {
            TransactionScope(() =>
            {
                //Arrange
                _userRepository.Add(user);

                //Act
                var actual = _userRepository.GetList();

                //Assert
                Assert.IsTrue(actual.Any());
            });
        }

        private static User GetUserTest()
        {
            return new User
            {
                Password = RandomGenerator.String(),
                Type = UserType.Guest,
                UserName = RandomGenerator.String()
            };
        }
    }
}