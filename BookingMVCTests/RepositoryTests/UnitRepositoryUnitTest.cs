﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Common;
using BookingMVC.Repositories;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests
{
    [TestClass]
    public class UnitRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private UnitRepository _unitRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            _unitRepository = new UnitRepository();
        }

        [TestMethod]
        public void AddUnit()
        {
            TransactionScope(() =>
                {
                    //Act
                    var expected = GetTestUnit();

                    using (var db = new UnitDbContext(BookingConnectionString))
                    {
                        string actual = db.Units.Where(l => l.Id == expected.Id).Select(l => l.Name).SingleOrDefault();

                        //Assert
                        Assert.AreEqual(expected.Name, actual);
                    }
                });
        }

        [TestMethod]
        public void EditUnit()
        {
            TransactionScope(() =>
                {
                    //Arrange
                    var unit = GetTestUnit();
                    Unit expected = new Unit
                    {
                        Id = unit.Id,
                        Address = RandomGenerator.String(),
                        Name = RandomGenerator.String(),
                        Type = UnitType.Hotel,
                        Contact = RandomGenerator.Integer(25, 1000).ToString()
                    };

                    //Act
                    _unitRepository.Edit(expected);

                    using (var db = new UnitDbContext(BookingConnectionString))
                    {
                        string actual = db.Units.Where(l => l.Id == expected.Id).Select(l => l.Name).SingleOrDefault();

                        //Assert
                        Assert.AreEqual(expected.Name, actual);
                    }
                });
        }

        [TestMethod]
        public void DeleteUnit()
        {
            TransactionScope(() =>
             {
                 var unit = GetTestUnit();

                 //Act
                 _unitRepository.DeleteUnit(unit.Id);

                 using (var db = new UnitDbContext(BookingConnectionString))
                 {
                     //Assert
                     var expected = db.Units.Where(l => l.Id == unit.Id).SingleOrDefault();

                     Assert.IsNull(expected);
                 }
             });
        }

        [TestMethod]
        public void ShowAllUnitsFromDatabase()
        {
            TransactionScope(() =>
            {
                //Act
                var actual = _unitRepository.GetList();

                //Assert
                Assert.IsTrue(actual.Any());
            });
        }

        [TestMethod]
        public void GetByName_ReturnUnit_IfNameExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var expected = GetTestUnit();

                //Act
                var actual = _unitRepository.GetName(expected.Name);

                //Assert
                Assert.AreEqual(expected.Id, actual.Id);
            });
        }

        [TestMethod]
        public void GetByName_ReturnNull_IfNameDoesNotExists()
        {
            TransactionScope(() =>
                {
                    //Arrange
                    var unit = GetTestUnit();
                    _unitRepository.DeleteUnit(unit.Id);

                    // Act
                    var actual = _unitRepository.GetName(unit.Name);

                    //Assert
                    Assert.IsNull(actual);
                });
        }


        [TestMethod]
        public void GetBySearch_ReturnUnit_IfUnitExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();

                //Act
                var actual = _unitRepository.GetUnits(unit.Name);

                //Assert
                Assert.IsTrue(actual.Any());
            });
        }

        [TestMethod]
        public void GetBySearch_ReturnNull_IfUnitDoesNotExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                _unitRepository.DeleteUnit(unit.Id);

                //Act
                var actual = _unitRepository.GetUnits(unit.Address);

                //Assert
                Assert.IsFalse(actual.Any());
            });
        }

        [TestMethod]
        public void GetNameById_ReturnName_IfUnitExist()
        {
            TransactionScope(() =>
            {
                //Arrange
                var expected = GetTestUnit();

                //Act
                var actual = _unitRepository.GetNameById(expected.Id);

                //Assert
                Assert.AreEqual(expected.Name, actual);

            });
        }

        [TestMethod]
        public void GetById_ReturnUnit_IfUnitExist()
        {
            TransactionScope(() =>
            {
                //Arrange
                var expected = GetTestUnit();

                //Act
                var actual = _unitRepository.GetById(expected.Id);

                //Assert
                Assert.AreEqual(expected.Name, actual.Name);
            });
        }

        [TestMethod]
        public void GetById_ReturnNull_IfUnitDoesNotExists()
        {
            //Arrange
            const int id = -100;

            TransactionScope(() =>
            {
                //Act
                var actual = _unitRepository.GetById(id);

                //Assert
                Assert.IsNull(actual);
            });
        }
    }
}