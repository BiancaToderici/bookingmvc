﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BookingMVCTests.RepositoryTests
{
    public static class RandomGenerator
    {
        private static Random random = new Random();

        public static string String([Optional] int optionalLength)
        {
            const int defaultLength = 10;
            int length = optionalLength == 0 ? defaultLength : optionalLength;

            return Builder(length);
        }

        private static string Builder(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            var builder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var c = chars[random.Next(0, chars.Length)];
                builder.Append(c);
            }
            return builder.ToString();
        }

        public static byte[] ByteArray([Optional] int optionalSize)
        {
            const int defaultSize = 5;
            int size = optionalSize == 0 ? defaultSize : optionalSize;
            Byte[] b = new byte[size * 1024];
            random.NextBytes(b);
            return b;
        }

        public static int Integer(int min, int max)
        {
            var number= min > max ? random.Next(max, min) : random.Next(min, max);
            return number;
        }

        public static DateTime Date(DateTime dateTime)
        {
            DateTime start = new DateTime(2019, 1, 1);
            int range = (dateTime - start).Days;
            return start.AddDays(random.Next(range));
        }
    }
}
