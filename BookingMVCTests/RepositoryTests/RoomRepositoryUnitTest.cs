﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests.RepositoryTests
{
    [TestClass]
    public class RoomRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private RoomRepository _roomRepository;
        private RoomBookingRepository _roomBookingRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            _roomRepository = new RoomRepository();
            _roomBookingRepository = new RoomBookingRepository();
        }

        [TestMethod]
        public void Add()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();

                //Act
                var expected = GetTestRoom(unit.Id);

                using (var db = new RoomDbContext(BookingConnectionString))
                {
                    var actual = db.Rooms.Where(r => r.Id == expected.Id).SingleOrDefault();

                    //Assert
                    Assert.AreEqual(expected.Type, actual.Type);
                    Assert.AreEqual(expected.RoomNr, actual.RoomNr);
                }
            });
        }

        [TestMethod]
        public void Edit()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                Room expected = new Room
                {
                    Id = room.Id,
                    UnitId = room.UnitId,
                    RoomNr = RandomGenerator.Integer(5,10),
                    Type = RandomGenerator.String(),
                    Price = RandomGenerator.Integer(100,250)
                };

                //Act
                _roomRepository.Edit(expected);

                using (var db = new RoomDbContext(BookingConnectionString))
                {
                    var actual = db.Rooms.Where(r => r.Id == expected.Id).SingleOrDefault();

                    //Assert

                    Assert.AreEqual(expected.Type, actual.Type);
                    Assert.AreEqual(expected.RoomNr, actual.RoomNr);
                }
            });
        }

        [TestMethod]
        public void Delete()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);

                //Act
                _roomRepository.Delete(room.Id);

                using(var db = new RoomDbContext(BookingConnectionString))
                {
                    var actual = db.Rooms.Where(r => r.Id == room.Id).SingleOrDefault();

                    //Assert
                    Assert.IsNull(actual);
                }
            });
        }

        [TestMethod]
        public void GetRooms_ShowsRoomsFromDatabase()
        {
            //Arrange
            var unit = GetTestUnit();
            var expected = GetTestRoom(unit.Id);

            //Act
            var actual = _roomRepository.GetRooms(unit.Id);

            //Assert
            Assert.AreEqual(expected.Id, actual.First().Id);
            Assert.IsTrue(actual.Any());
        }

        [TestMethod]
        public void GetRoom_ReturnRoom_IfIdExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var expected = GetTestRoom(unit.Id);

                using ( var db = new RoomDbContext(BookingConnectionString))
                {
                    //Act
                    var actual = _roomRepository.GetById(expected.Id);

                    //Assert
                    Assert.AreEqual(expected.Id, actual.Id);
                }
            });
        }

        [TestMethod]
        public void GetRoom_ReturnNull_IfIdDoesNotExists()
        {
            //Arrange
            const int id = -1000;

            //Act
            var actual = _roomRepository.GetById(id);

            //Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetNr_ReturnRoom_IfIdAnRoomNrExists()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var expected = GetTestRoom(unit.Id);

                using (var db = new RoomDbContext(BookingConnectionString))
                {
                    //Act
                    var actual = _roomRepository.GetNr(expected.RoomNr, expected.UnitId);

                    //Assert
                    Assert.AreEqual(expected.RoomNr, actual.RoomNr);
                }
            });
        }

        [TestMethod]
        public void GetNr_ReturnNull_IfIdAnRoomNrDoesNotExists()
        {
            //Arrange
            const int roomNr = -1001;

            TransactionScope(() =>
            {
                var unit = GetTestUnit();
                var expected = GetTestRoom(unit.Id);

                using (var db = new RoomDbContext(BookingConnectionString))
                {
                    //Act
                    var actual = _roomRepository.GetNr(roomNr, expected.Id);

                    //Assert
                    Assert.IsNull(actual);
                }
            });
        }

        [TestMethod]
        public void GetRoomNr_ReturnNumber_IfIdExists()
        {
            TransactionScope(() =>
            {
                var unit = GetTestUnit();
                var expected = GetTestRoom(unit.Id);

                using (var db = new RoomDbContext(BookingConnectionString))
                {
                    //Act
                    var actual = _roomRepository.RoomNr(expected.Id);

                    //Assert
                    Assert.AreEqual(expected.RoomNr, actual);
                }
            });
        }
        
        [TestMethod]
        public void GetRoomNr_ReturnNull_IfIdDoesNotExists()
        {
            //Arrange
            const int id = -1001;

           //Act
           var actual = _roomRepository.RoomNr(id);

            //Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void IsBooked_ReturnTrue_IfRoomIsBooked()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var booking = GetTestRoomBooking(room.Id);

                //Act
                var actual = _roomRepository.IsBooked(room, booking.DateFrom, booking.DateTo);

                //Assert
                Assert.IsTrue(actual);
            });
        }

        [TestMethod]
        public void IsBooked_ReturnFalse_IfRoomIsNotBooked()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var dateTo = RandomGenerator.Date(DateTime.Now);
                var dateFrom = RandomGenerator.Date(dateTo);

                //Act
                var actual = _roomRepository.IsBooked(room, dateFrom, dateTo);

                //Assert
                Assert.IsFalse(actual);
            });
        }

        [TestMethod]
        public void GetFreeRooms_ReturnRooms_IfRoomsAreNotBooked()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var dateTo = RandomGenerator.Date(DateTime.Now);
                var dateFrom = RandomGenerator.Date(dateTo);
                

                //Act
                var freeRooms = _roomRepository.GetFreeRooms(dateFrom, dateTo, room.UnitId);

                //Assert
                var actual = freeRooms;
                Assert.IsTrue(actual.Any());
            });
        }
    }
}
