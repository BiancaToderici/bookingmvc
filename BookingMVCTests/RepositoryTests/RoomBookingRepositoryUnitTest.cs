﻿using System;
using System.Linq;
using System.Transactions;
using BookingMVC.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BookingMVCTests.RepositoryTests
{
    [TestClass]
    public class RoomBookingRepositoryUnitTest : BaseRepositoryUnitTest
    {
        private RoomBookingRepository _roomBookingRepository;

        [TestInitialize]
        public void TestInitialize()
        {
            _roomBookingRepository = new RoomBookingRepository();
        }
        
        [TestMethod]
        public void ShowAllRoomsBooked()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);
                var expected = GetTestRoomBooking(room.Id);

                //Act
                var actualBookings = _roomBookingRepository.GetRoomsBooked();

                //Assert
                var actual = actualBookings.SingleOrDefault(b => b.Id == expected.Id);
                Assert.AreEqual(expected.RoomId, actual.RoomId);
            });
           
        }

        [TestMethod]
        public void BookARoom()
        {
            TransactionScope(() =>
            {
                //Arrange
                var unit = GetTestUnit();
                var room = GetTestRoom(unit.Id);

                //Act
                var expected = GetTestRoomBooking(room.Id);

                using (var db = new RoomBookingDbContext(BookingConnectionString))
                {
                    var actual = db.RoomsBooking.Where(r => r.Id == expected.Id).SingleOrDefault();

                    //Assert
                    Assert.AreEqual(expected.Id, actual.Id);
                }
            });
        }
    }
}
