﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BookingMVC.Common;
using BookingMVC.Models;
using BookingMVC.Repositories;
using BookingMVC.Repositories.Contracts;
using BookingMVC.Services;
using BookingMVCTests.RepositoryTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BookingMVCTests
{
    [TestClass]
    public class HistoryServiceUnitTest
    {
        private Mock<IHistoryRepository> _mockHistoryRepository;
        private Mock<IRatingRepository> _mockRatingRepository;
        private HistoryService _historyService;

        [TestInitialize]
        public void TeestInitialize()
        {
            _mockHistoryRepository = new Mock<IHistoryRepository>();
            _mockRatingRepository = new Mock<IRatingRepository>();
            _historyService = new HistoryService(_mockHistoryRepository.Object, _mockRatingRepository.Object);
        }

        [TestMethod]
        public void PopulateHistoryDatabase_Verify()
        {
            //Arrange
            var data = GetModel();

            //Act
            _historyService.RoomHistory(data);

            //Assert
            _mockHistoryRepository.Verify(h => h.SaveHistory(It.IsAny<History>()));
        }

        [TestMethod]
        public void ShowHistoryOfBookingsForUsers_ReturnsBookings_IfUserIdExists()
        {
            //Arrange
            var listOfBookings = GetHistory();
            _mockHistoryRepository.Setup(d => d.History(It.IsAny<int>())).Returns(listOfBookings);

            //Act
            var actual = _historyService.RoomsBooked(It.IsAny<int>());

            //Assert
            Assert.AreEqual(2, actual.Count());
        }

        [TestMethod]
        public void ShowHistoryOfBookingsForUsers_ReturnsZero_IfUserIdDoesntEists()
        {
            //Arrange
            _mockHistoryRepository.Setup(d => d.History(It.IsAny<int>())).Returns(new List<RoomBookingHistory>());

            //Act
            var actual = _historyService.RoomsBooked(It.IsAny<int>());

            //Assert
            Assert.IsFalse(actual.Any());
        }

        public static HistoryModel GetModel()
        {
            return new HistoryModel()
            {
                RoomId = RandomGenerator.Integer(1,3),
                UserId = RandomGenerator.Integer(1,9)
            };
        }

        public static List<RoomBookingHistory> GetHistory()
        {
            var listOfDetails = new List<RoomBookingHistory>();

            var book = new RoomBookingHistory()
            {
                RoomBooking = new RoomBooking(),
                Room = new Room(),
                Unit = new Unit()
            };

            var book1 = new RoomBookingHistory()
            {
                RoomBooking = new RoomBooking(),
                Room = new Room(),
                Unit = new Unit()
            };

            listOfDetails.Add(book);
            listOfDetails.Add(book1);
            return listOfDetails;
        }
    }
}
